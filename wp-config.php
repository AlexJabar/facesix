<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'newfa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cj[DyZJNoOudmc^j3QHfq}?6Aqhr_LZ,g`YzLNxs3 rL({`9CvR6$m6]{EY;^tFE');
define('SECURE_AUTH_KEY',  '=AcBW4``x{A$4S2Kh-LvdZS75c,LtEK:_wnuy9%$ga6~DI%ZSg^jrqbJ#v!sTL0W');
define('LOGGED_IN_KEY',    'KCD 0iic6BIQ~wz;)y=X&AO~nhM)SL/:tVNQqej}8wAZ-b$E5iT`~Zj1mlcM{li)');
define('NONCE_KEY',        'FAHrZ%5s}~kJ6>M+,DfOw#gKR)rmg+E#R|*}3O%^b@a2P`&R1i5~]uBR)/,Wj[Xt');
define('AUTH_SALT',        'l>gqQl}Y!S]_.KB>`}s]+>4m:aF~`sYY.[lW,Lr)S/zy<q_lXh_tU6a {%d!ndy7');
define('SECURE_AUTH_SALT', 'Ln4h8>+)dr&Pk-lO_77x-68z+:mn-in:mk,P8W-t-Ey`,#fuCx{sP8~ABt-0>Z:)');
define('LOGGED_IN_SALT',   '9#N:wF%sHm~v.j@v@BH/o}vV<,Fx-r7j@. rSWL;*BPijK2zdn7;Ax,mx$x7#3|V');
define('NONCE_SALT',       '(tz,(0C}:eB6B #Ci0wRnD6@fQ>R1TSzX}shU.U0O(jm3*t~}nmy3ag`K1f1>`= ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/*define('WP_HOME', 'http://0960c94e.ngrok.io/test/');
define('WP_SITEURL', 'http://0960c94e.ngrok.io/test/');*/


define('WP_HOME', 'http://localhost/test/');
define('WP_SITEURL', 'http://localhost/test/');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
