<?php

/**

 * Home

 *

 * Standard loop for the blog-page

 */

 get_header(); ?>

<?php $page_top_banner = get_field('page_top_banner', get_option('page_for_posts')); ?>
	<div class="default-page blog-page <?php echo $page_top_banner ? '' : 'without-banner' ?>">
		<?php if ($page_top_banner): ?>
			<div class="top-banner banner-app" <?php bg($page_top_banner, 'full_hd') ?>>
				<?php if ($top_banner_title = get_field('page_top_banner_title')): ?>
					<h2 class="top-banner-title"><?php echo $top_banner_title; ?></h2>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<?php $posts_to_exclude = array(); ?>
		<div class="blog-main-posts">
			<div class="row large-7 medium-9 small-12">
				<?php $main_post_arg = array(
					'post_type' => 'post',
					'order' => 'ASC',
					'orderby' => 'menu_order',
					'posts_per_page' => -1,
					'cat' => '9'
				);
				$main_post_query = new WP_Query($main_post_arg);
				if ($main_post_query->have_posts()) : ?>
					<div class="large-8 medium-6 small-12 columns blog-post-col main-post">
						<?php while ($main_post_query->have_posts()) : $main_post_query->the_post();
							array_push($posts_to_exclude, get_the_ID()); ?>
							<?php
							$cat_arg = array('hide_empty' => true, 'exclude' => array(1, 9, 10));
							$main_post_cats = get_categories($cat_arg); ?>
							<!--<div class="post-data">
								<p class='text-center'>
								<span>
									<?php foreach ($main_post_cats as $post_cat): ?>
										<?php echo $post_cat->name . ' '; ?>
									<?php endforeach; ?>
								</span>
								<span>
									<?php echo get_the_date('d.j.Y '); ?>
								</span>
								</p>
							</div>-->
							<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
							<div class="thumb-container" <?php //bg(get_attached_img_url(get_the_ID()), 'large') ?>>
								<?php the_post_thumbnail('large') ?>
								<?php echo get_featured_image_caption()?>
							</div>
							<div class="post-excerpt"><?php the_excerpt(); ?></div>
							<!--<div class="post-author-comment-container">
								<p class="post-author text-left">by <?php the_author(); ?></p>
								<p class="post-comments-quantity"><?php comments_number(); ?></p>
							</div>-->
						<?php endwhile; ?>
					</div>
				<?php endif;
				wp_reset_query(); ?>
				<?php $secondary_post_arg = array(
					'post_type' => 'post',
					'order' => 'DESC',
					'orderby' => 'date',
					'posts_per_page' => -1,
					'cat' => '10'
				);
				$secondary_post_query = new WP_Query($secondary_post_arg);
				if ($secondary_post_query->have_posts()) : ?>
					<div class="columns blog-post-col">
						<?php while ($secondary_post_query->have_posts()) : $secondary_post_query->the_post();
							array_push($posts_to_exclude, get_the_ID()); ?>
							<?php
							$cat_arg = array('hide_empty' => true, 'exclude' => array(1, 9, 10));
							$main_post_cats = get_categories($cat_arg); ?>
							<div class="secondary-posts">
								<!--<div class="post-data">
									<p class='text-center'>
									<span>
										<?php foreach ($main_post_cats as $post_cat): ?>
											<?php echo $post_cat->name . ' '; ?>
										<?php endforeach; ?>
									</span>
									<span>
										<?php echo get_the_date('d.j.Y '); ?>
									</span>
									</p>
								</div>-->
								<h5 class="text-center"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
								<div class="thumb-container" <?php //bg(get_attached_img_url(get_the_ID()), 'large') ?>>
									<?php the_post_thumbnail('large') ?>
									<?php echo get_featured_image_caption()?>
								</div>
								<div class="post-excerpt"><?php the_excerpt(); ?></div>
								<!--<div class="post-author-comment-container">
									<p class="post-author">by <?php the_author(); ?></p>
									<p class="post-comments-quantity"><?php comments_number(); ?></p>
								</div>-->
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif;
				wp_reset_query(); ?>
			</div>
		</div>
		<div class="blog-last-posts">
			<!--<div class="delimeter-title">
				<h1 class="text-center"><?php _e('Know more about Face Recognition', 'face-six') ?></h1>
			</div>-->
			<div class="row">
				<?php $last_post_arg = array(
					'post_type' => 'post',
					'order' => 'DESC',
					'orderby' => 'menu_order',
					'posts_per_page' => 6,
					'post__not_in' => $posts_to_exclude
				);
				$last_post_query = new WP_Query($last_post_arg);
				if ($last_post_query->have_posts()) : ?>
					<?php while ($last_post_query->have_posts()) : $last_post_query->the_post();
						array_push($posts_to_exclude, get_the_ID()); ?>
						<?php
						$cat_arg = array('hide_empty' => true, 'exclude' => array(1, 9, 10));
						$main_post_cats = get_categories($cat_arg); ?>
						<div class="large-4 medium-6 small-12 columns blog-post-col matchHeight">
							<!--<div class="post-data">
								<p class='text-center'>
									<span>
										<?php foreach ($main_post_cats as $post_cat): ?>
											<?php echo $post_cat->name . ' '; ?>
										<?php endforeach; ?>
									</span>
									<span>
										<?php echo get_the_date('d.j.Y '); ?>
									</span>
								</p>
							</div>-->
							<h6><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h6>
							<div class="post-excerpt"><?php the_excerpt(); ?></div>
							<!--<div class="post-author-comment-container">
								<p class="post-author text-left">by <?php the_author(); ?></p>
								<p class="post-comments-quantity"><?php comments_number(); ?></p>
							</div>-->
						</div>
					<?php endwhile; ?>
				<?php endif;
				wp_reset_query(); ?>
				<?php if (wp_count_posts('post')->publish > 9): ?>
					<?php $implode_array = implode(',', $posts_to_exclude); ?><?php echo do_shortcode('[ajax_load_more post_type="post" posts_per_page="9" pause="true" scroll="false" transition="fade"   button_label="See More Posts"  post__not_in="' . $implode_array . '"]'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>