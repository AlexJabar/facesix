<?php
/**
 * Created by PhpStorm.
 * User: HACKER
 * Date: 06.03.2019
 * Time: 12:11
 */


?>


<div class="columns small-12 shop-options-container">
    <div class="row">
        <div class="small-6 columns shop-options-picker-section">
            <h2 class="shop-info-subscr-det">Subscription Details</h2>

            <div class="info-plan-label">Application:</div>
            <div class="app-label-span"></div>
            <br>
            <div class="info-plan-label">Plan:</div>
            <div class="plan-label-span"></div>

            <div class="period-switch-label">Subscription period</div>
            <fieldset>
                <div class="switch-toggle switch-candy">
                    <input id="month" name="view" type="radio">
                    <label for="month" onclick="">Month</label>

                    <input id="year" name="view" type="radio">
                    <label for="year" onclick="">Year </label>

                    <a></a>
                </div>
            </fieldset>
            <div class="toggle-discount-info">Save up to 20%</div>

            <?php
            do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' );
            ?>

        </div>
        <div class="small-6 columns shop-info-section">
            <div class="shop-info-product-thumbnail">
                <?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>

                <div class="shop-info-product-thumbnail-title"><?php the_title(); ?></div>
                <!--<div class="shop-info-product-thumbnail-plan"></div>-->

            </div>

            <div class="shop-info-main-attrs">

            </div>


        </div>


    </div>
</div>

