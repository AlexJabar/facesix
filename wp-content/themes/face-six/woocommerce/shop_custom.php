<?php
/**
 * Created by PhpStorm.
 * User: HACKER
 * Date: 10.02.2019
 * Time: 11:32
 */



/**
 * @snippet       Add Custom Field to Product Variations - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=73545
 * @author        Rodolfo Melogli
 * @compatible    Woo 3.3.4
 */


add_action('woocommerce_before_add_to_cart_quantity', 'add_title_single_product_button');

function add_title_single_product_button() {
    ?>
    <div class="price-per-camera-container">
        <div class="label-before-camera-quantity">Price per camera</div>
        <div class="price-per-camera-amount"></div>
    </div>
    <div class="quantity-container">
    <div class="label-before-camera-quantity">Cameras Amount</div>
        <div class="quantity-controls-container">
            <button type="button" class="shop-quantity-dec-button">&#8722;</button>
    <?php
}


add_action('woocommerce_after_add_to_cart_quantity','add_dynamic_price');


function add_dynamic_price() {
    ?>
            <button type="button" class="shop-quantity-inc-button">&#43;</button>
        </div>
    </div>
    <div class="shop-variation-dynamic-price">
        <div class="shop-variation-dynamic-price-container">
            <div class="shop-variation-dynamic-price-text">Base price</div>
            <div class="shop-variation-dynamic-price-text price-output-area"></div>
        </div>
        <div class='shop-variation-dynamic-price-discount-container'>
            <div class="shop-variation-dynamic-price-text text-discount-output-area"></div>
            <div class="shop-variation-dynamic-price-text discount-output-area"></div>
        </div>
        <div class='shop-variation-dynamic-price-fee-container'>
            <div class="shop-variation-dynamic-price-text"></div>
            <div class="shop-variation-dynamic-price-text fee-output-area"></div>
        </div>
        <div class="shop-variation-dynamic-price-total-container">
            <div class="shop-variation-dynamic-price-text">Total price</div>
            <div class="shop-variation-dynamic-price-text total-output-area"></div>
        </div>

    </div>

    <?php
}



// ****************************************************************************************************************
// 1. Add custom field input @ Product Data > Variations > Single Variation

add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 );

function bbloomer_add_custom_field_to_variations( $loop, $variation_data, $variation ) {

///////////////////////////////////////////////////////////Constant parameters :

woocommerce_wp_text_input( array(
        'id' => 'custom_field[' . $loop . '][0]',
        'name' => "key[{$loop}][0]",
        'class' => 'short',
        'type' => 'hidden', // for constant parameter
        'value' => 'No. Of Visits' // for constant parameter
    )
);

    woocommerce_wp_text_input( array(
            'id' => 'custom_field[' . $loop . '][0]',
            'name' => "value[{$loop}][0]",
            'class' => 'short',
            'label' => __( 'No. Of Visits', 'woocommerce' ),
            'value' => array_values(json_decode(get_post_meta( $variation->ID, 'customFields', true ),true))[0]
        )
    );

    woocommerce_wp_text_input( array(
            'id' => 'custom_field[' . $loop . '][1]',
            'name' => "key[{$loop}][1]",
            'class' => 'short',
            'type' => 'hidden', // for constant parameter
            'value' => 'No. Of People in DB' // for constant parameter
        )
    );

    woocommerce_wp_text_input( array(
            'id' => 'custom_field[' . $loop . '][1]',
            'name' => "value[{$loop}][1]",
            'class' => 'short',
            'label' => __( 'No. Of People in DB', 'woocommerce' ),
            'value' => array_values(json_decode(get_post_meta( $variation->ID, 'customFields', true ),true))[1]
        )
    );


///////////////////////////////////////////////////////////Variable parameters :
    woocommerce_wp_text_input( array(
            'id' => 'custom_field[' . $loop . '][2]',
            'name' => "key[{$loop}][2]",
            'class' => 'short',
            'label' => __( 'Key', 'woocommerce' ),
            'value' => array_keys(json_decode(get_post_meta( $variation->ID, 'customFields', true ),true))[2]
        )
    );

    woocommerce_wp_text_input( array(
            'id' => 'custom_field[' . $loop . '][2]',
            'name' => "value[{$loop}][2]",
            'class' => 'short',
            'label' => __( 'Value', 'woocommerce' ),
            'value' => array_values(json_decode(get_post_meta( $variation->ID, 'customFields', true ),true))[2]
        )
    );



}

// 2. Save custom field on product variation save

add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations', 10, 2 );

function bbloomer_save_custom_field_variations( $variation_id, $i ) {
    $custom_values = $_POST['value'][$i];
    $custom_keys = $_POST['key'][$i];

    update_post_meta( $variation_id,  'customFields' , json_encode(array_combine($custom_keys, $custom_values)));
}

 // ****************************************************************************************************************




add_action( 'woocommerce_before_checkout_form', 'bbloomer_cart_on_checkout_page_only', 5 ); //make cart and checkout on same page

function bbloomer_cart_on_checkout_page_only() {
    if ( is_wc_endpoint_url( 'order-received' ) ) return;
    echo do_shortcode('[woocommerce_cart]');
}


add_filter( 'woocommerce_subscriptions_calculated_total', 'single_sign_up_fee_per_item' ); // make sign up fee only one per cart item

function single_sign_up_fee_per_item($total) {

    foreach ( WC()->cart->cart_contents as $item ){
        $suf = $item['data']->get_sign_up_fee();
    }

    return (isset($suf)) ? $total + $suf :  $total;
}

    add_filter( 'wc_add_to_cart_message_html', '__return_null' ); // turn off woocommerce notices



    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form'); // move coupon field
    add_action( 'woocommerce_review_order_before_payment', 'woocommerce_checkout_coupon_form', 1 );


    remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );



?>


