<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
global $product;
$attribute = $product->get_attributes()['pa_plan'];

///////////////////////////////////////////////////////////Get discount rules: (that use plugin  woocommerce-discounts-plus)
$pda = get_post_meta( $product->get_id() );
$plus_discount_arr = array();
if(!empty($pda)){
    foreach($pda as $k=>$v){
        $pdq = substr($k, 0, strlen('plus_discount_quantity_'))=='plus_discount_quantity_';
        if(substr($k, 0, strlen('plus_discount_'))!='plus_discount_'){
            unset($pda[$k]);
        }elseif($pdq){
            $qty = current($v);
            if($qty>0){
                $ku = str_replace('plus_discount_quantity_', 'plus_discount_discount_'.($is_flat?'flat_':''), $k);
                $vu = (isset($pda[$ku])?current($pda[$ku]):0);
                $plus_discount_arr[$qty] = $vu;
            }
        }
    }

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$args = array(
    'post_type' => 'product_variation',
    'post_status' => array('private', 'publish'),
    'numberposts' => -1,
    'orderby' => 'menu_order',
    'order' => 'asc',
    'post_parent' => get_the_ID() // get parent post-ID
);
$variations = get_posts($args);
$custom_fields = json_decode($variations[0]->customFields, true);


$outputed_slugs = [];
$unique_vars = [];
$sign_up_fees = [];
?>


<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
    <div class="row shop-row">
        <div class="columns small-12 shop-plan-variation-container">
            <div class="plan-header-title">Choose a plan that's right for you.</div>
            <div class="shop-plan-variation-header">
                <div class="shop-plan-variation-price-title">
                    Base monthly price
                </div>
                <div class="plans-wrapper">

                    <?php
                    foreach ($variations as $key => $variation) :
                        $sign_up_fees[$variation->ID] = $variation->_subscription_sign_up_fee; // get array of fees

                        if (!in_array($variation->attribute_pa_plan, $outputed_slugs)) {
                            $outputed_slugs[] = $variation->attribute_pa_plan;
                            $unique_vars[] = $variation;
                            ?>
                            <div class="plan-name-container">
                                <div class="plan-name-box" id="plan-name-id-<?= $variation->attribute_pa_plan ?>">
                                    <?= ucfirst($variation->attribute_pa_plan) ?>
                                </div>
                                <div class="arrow-box">
                                    <div class="arrow-down plan-group-<?= $variation->attribute_pa_plan ?>"></div>
                                </div>
                                <div class="plan-price-box plan-group-<?= $variation->attribute_pa_plan ?>">
                                    <?= $variation->_subscription_price . "$" ?>
                                </div>
                            </div>
                            <?php
                        }
                    endforeach;
                    ?>
                </div>
            </div>
            <div class="shop-plan-variation-body">


                <?php
                foreach ($custom_fields as $key => $custom_field) :
                    ?>
                    <div class="testy-test">
                        <div class="shop-plan-variation-customfields-titles-row">
                            <?= $key ?>
                        </div>
                        <div class="shop-plan-variation-customfields-row">
                            <?php
                            foreach ($unique_vars as $variation) :
                                ?>
                                <div class="shop-plan-variation-customfields-col plan-group-<?=$variation->attribute_pa_plan?>">
                                    <?php
                                    if (isset(json_decode($variation->customFields, true)[$key])) {
                                        if (json_decode($variation->customFields, true)[$key] == '+')
                                            echo '&#x2714;';
                                        elseif (json_decode($variation->customFields, true)[$key] == '-' ||
                                                json_decode($variation->customFields, true)[$key] == '')
                                            echo '&#10007;';
                                        else
                                            echo json_decode($variation->customFields, true)[$key];

                                    }
                                    else echo '&#10008;';
                                    ?>
                                </div>
                            <?php
                            endforeach;
                            ?>
                        </div>
                    </div>
                <?php
                endforeach;
                ?>
            </div>
            <div class="shop-plan-variation-continue signup_button_wrapper">
                <button class="signup_button" id="plan-first-continue"><a>CONTINUE</a></button>
            </div>
        </div>
        <div class="columns small-12 shop-loader-container"></div>




    </div>


</div>

<script type="text/javascript">
    var fees      = <?= json_encode($sign_up_fees)?>;
    var disc_data = <?= json_encode($plus_discount_arr)?>;
</script>


<?php include 'final-product-options.php';?>




