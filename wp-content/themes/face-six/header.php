<?php
/**
 * Header
 */
?>
<!DOCTYPE html>
<!--[if !(IE)]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<!--[if gte IE 9]>
<style type="text/css">
	.gradient {
		filter: none;
	}
</style>
<![endif]-->

<head>
    <meta name="google-site-verification" content="YPrwJ15ly9xRlUAXysYnwHNboQHa1fkJwUwJsIJOmKs" />
	<!-- Set up Meta -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<!-- Add Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-47704866-1', 'face-six.com');
  ga('send', 'pageview');
</script>
<!-- BEGIN of header -->
<header class="header">
	<div class="header__top">
		<div class="row">
			<div class=" small-12 columns text-right">
				<ul class="header__language-selector">
				<!--	<li class="header__language english-language"><a href="#">EN <img src="<?php echo get_template_directory_uri() ?>/images/english-flag.jpg"></a></li>-->
				</ul>
				<?php if(!is_user_logged_in() ) {?>
				<ul class="header__login">
				  
					<li class="sign-up"><a href="<?php echo wp_registration_url(); ?>"><?php _e('Sign Up','face-six') ?></a></li>
					<li class="login"><a href=" <?php echo wp_login_url(); ?>"><?php _e('Log In','face-six') ?></a></li>
				</ul>
			<?php } else { ?>
				<div class="user_info pull-right">
					<?php 
						$cur_user_id = get_current_user_id();
						$user = get_userdata($cur_user_id);

						//print_r($user);
					?>
					
					<div class="dropdown pull-right">
						<ul class="vertical menu accordion-menu nested" data-accordion-menu data-slide-speed="10">
							<li> <a href="#"><span class="user_name">HI <?php echo $user->user_nicename; ?></span><?php echo get_avatar( $cur_user_id, 32 ); ?></a>
								<ul style="display: none;"  class="menu vertical nested">
									<li><a href="<?php echo site_url() . '/member-account'; ?>">Your Page</a></li>
									<li><a href="<?php echo site_url() . '/user-info'; ?>">Personal info</a></li>
									<li><a href="<?php echo site_url() . '/account-settings'; ?>">Settings</a></li>
									<?php 
										$cur_user_id = get_current_user_id();
										$user_info = get_userdata( $cur_user_id );
										$user_role =  implode(', ', $user_info->roles);
										if($user_role == 'shop_manager'):
									?> 
									<li><a href="<?php echo site_url() . '/parhner-page'; ?>">Parthner page</a></li>
									<?php endif; ?>
									
									<li><a href="<?php echo wp_logout_url(); ?>">Logout</a></li>
								</ul>
							</li>
						</ul>  
					</div>
				</div>
			<?php } ?>
				<span id="invitation" style=" position: absolute; left: -40%;white-space: nowrap;"><a href="https://cyberweek.tau.ac.il/2018/Events/BIST-2018" target="_blank" style="color:white; font-size:1.2em;">Meet us at our booth at <span style="text-decoration: underline;">"BIST 2018"</span>, June 19th in Tel Aviv</a></span>
<span style=" position: absolute; right: 0; padding: 0 10px; background: #b41c17; color: white;">בס''ד</span>	
			</div>
		</div>
	</div>
	<div class="header__bottom">
		<div class="row large-uncollapse small-collapse">
			<div class="large-3 small-12 columns">
				<div class="logo text-center medium-text-left">
					<?php show_custom_logo(); ?>
				</div>
			</div>
			<div class="large-9 small-12 columns">
				<div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium">
					<button class="menu-icon" type="button" data-toggle></button>
					<div class="title-bar-title">Menu</div>
				</div>
				<nav class="top-bar" id="main-menu">
					<?php
					if (has_nav_menu('header-menu')) {
						wp_nav_menu(array('theme_location' => 'header-menu',
							'menu_class' => 'menu header-menu dropdown',
							'items_wrap' => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown" data-close-on-click-inside="false">%3$s</ul>',
							'walker' => new Foundation_Navigation()));
					}
					?>
				</nav>
			</div>
		</div>
	</div>
</header>

<!-- END of header -->
