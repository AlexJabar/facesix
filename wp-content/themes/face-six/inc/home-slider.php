<?php
// Create HOME Slider
function home_slider_template() { ?>



	<?php $arg = array(
		'post_type'      => 'slider',
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
		'posts_per_page' => - 1
	);
	$slider    = new WP_Query( $arg );
	if ( $slider->have_posts() ) : ?>

		<div id="home-slider" class="slick-slider">
			<?php $i = 1; ?>
			<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>
				<div class="slick-slide" >
					<div class="slide-reverse" >
						<?php if($i != 1): ?>
							<div class="row columns">
								<div class="slide-button-container">
									<?php
										$slide_link = '#';
										if($slide_button_link = get_field('slide_button_link')){
											$slide_link = $slide_button_link;
										}
									?>
									<div>
										<?php if($slide_button_top_text_line = get_field('slide_button_top_text_line')): ?>
											<span class="top-button-text"><?php echo $slide_button_top_text_line ?></span>
										<?php endif; ?>
										<?php if($slide_button_bottom_text_line = get_field('slide_button_bottom_text_line')): ?>
											<span class="bottom-button-text"><?php echo $slide_button_bottom_text_line ?></span>
										<?php endif; ?>

									</div>
									<?php if($i !=1): ?>
										<?php if($slide_logo = get_field('slide_logo')): ?>
											<img src="<?php echo $slide_logo['sizes']['medium'] ?>">
										<?php endif; ?>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
						<?php
							$designed_by_title = get_field('designed_by_title');
							$designed_by_items = get_field('designed_by_items');

							//$items_count = count($designed_by_items );
                            $items_count = 1;
							$main_features_title = get_field('main_features_title');
							$main_features_items = get_field('main_features_items');

							$class = '';
							if($items_count < 2){
								$class = "big-top-padding";
							}elseif($items_count < 4){
								$class = 'big-bottom-padding';
							}elseif($items_count < 6){
								$class = 'small-bottom-padding';
							}

						 ?>
						 <?php if($i != 1): ?>
							<div class="designed-featured-container">
								<?php if($designed_by_title || $designed_by_items): ?>
									<ul class="designed-list <?php echo $class; ?>">
										<li class="designed-title"><span><?php echo $designed_by_title ?></span></li>
										 <?php if( have_rows('designed_by_items') ): ?>
	     										   <?php while( have_rows('designed_by_items') ): the_row();
	            										$designed_title = get_sub_field('desinged_by_item'); ?>
												<li><span><?php echo $designed_title ; ?></span></li>

											<?php endwhile; ?>
										<?php endif; ?>
									</ul>
								<?php endif; ?>
								<?php if($main_features_title || $main_features_items): ?>
									<ul class="featured-list">
										<li class="featured-title"><span><?php echo $main_features_title ?></span></li>
										 <?php if( have_rows('main_features_items') ): ?>
	     										   <?php while( have_rows('main_features_items') ): the_row();
	            										$featured_title = get_sub_field('main_features_item'); ?>
												<li><span><?php echo $featured_title ; ?></span></li>
											<?php endwhile; ?>
										<?php endif; ?>
									</ul>
								<?php endif; ?>
							</div>
							<a class="slide-link" href="<?php echo $slide_link  ?>"></a>
						<?php endif; ?>
						<?php $slides_video = get_field('slides_video','options');?>
						<?php if($slides_video && $i == 1 ): ?>
							<video class="slide-video-container" id="theVideo" poster="https://www.face-six.com/wp-content/uploads/2018/05/poster-2.gif" autoplay muted='true' >
  								 <source src="<?php echo $slides_video['url'] ?>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' media="all">
							</video>
						<?php endif; ?>
						<?php if( $i != 1 ): ?>
							<div class="slide-image-container" <?php bg( get_attached_img_url( get_the_ID(), 'full_hd' ) ); ?>></div>
						<?php endif; ?>


						<!-- <div class="slider-caption">
							<h3><?php// the_title(); ?></h3>
							<?php //the_content(); ?>
						</div> -->
					</div>
				</div>
				<?php $i++; ?>
			<?php endwhile; ?>
		</div><!-- END of  #home-slider-->

	<?php endif;
	wp_reset_query(); ?>
<?php }

// HOME Slider Shortcode

function home_slider_shortcode() {
	ob_start();
	home_slider_template();
	$slider = ob_get_clean();

	return $slider;
}

add_shortcode( 'slider', 'home_slider_shortcode' );
