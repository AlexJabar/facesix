<?php
/**
 * Footer
 */
?>

<!-- BEGIN of footer -->

<footer class="footer">
	<div class="footer__top">
		<div class="row">
			<div class="large-7 medium-8  small-12 columns">
				<?php
				if (has_nav_menu('footer-menu')) {
					wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'footer-menu','depth'=>1));
				}
				?>
			</div>
			<?php if( have_rows('social_networks','options') ): ?>
				<div class="large-5 medium-4  small-12 columns">
					<ul class="social-contacts">
						<?php while( have_rows('social_networks','options') ): the_row();
							$social_icon = get_sub_field('social_network_icon');
							$social_link = get_sub_field('social_network_url');
							$social_type = 'link';
							if(strpos($social_link, '@') !== false){
								$social_type = 'mail';
							}
						 ?>
							<li>
								<a href="<?php echo $social_type == 'mail' ? 'mailto:' : ''; echo $social_link ?>" <?php echo $social_type == 'mail' ? '' : 'target="_blank"';  ?>><img src="<?php echo $social_icon['sizes']['thumbnail'] ; ?>"></a>
							</li>
						<?php endwhile; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if($copyright = get_field('copyright','options')): ?>
		<div class="copyright">
			<div class="row columns text-center">
				<?php echo $copyright; ?>
			</div>
		</div>
	<?php endif; ?>

</footer>
<!-- END of footer -->

<?php wp_footer(); ?>
</body>
</html>
