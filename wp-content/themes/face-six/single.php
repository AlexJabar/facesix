<?php
/**
 * Single
 *
 * Loop container for single post content
 */
get_header(); ?>
<div class="post-wrapper">
<div class="row">
	<!-- BEGIN of post content -->
	<div class="small-12 columns">
		<main class="main-content">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1 class="page-title"><?php the_title(); ?></h1>
							<div class="post-data">
								<!--<p class="post-author-date"><span><?php echo get_the_date('d.j.Y '); ?></span> | <span>by <?php the_author(); ?></span></p>
								<p class="comments-quantitty"><?php comments_number(); ?></p>
							</div>-->
						<?php if ( has_post_thumbnail()) : ?>
							<div title="<?php the_title_attribute(); ?>" class="thumbnail">
								<?php the_post_thumbnail('large'); ?>
							</div>
						<?php endif; ?>
						<?php the_content('',true); ?>
						<div class="row">
							<div class="large-7 columns comments-col">
							<?php  comments_template(); ?>
							</div>
						</div>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
		</main>
	</div>
	<!-- END of post content -->
</div>
</div>
<?php get_footer(); ?>