;
(function($) {

    // Sticky Footer
    var bumpIt = function() {
            $('body').css('padding-bottom', $('.footer').outerHeight(true));
            $('.footer').addClass('sticky-footer');
        },
        didResize = false;

    $(window).resize(function() {
        didResize = true;
    });
    setInterval(function() {
        if (didResize) {
            didResize = false;
            bumpIt();
        }
    }, 250);

    // Scripts which runs after DOM load

    $(document).ready(function() {
        $('#home-slider').slick({
            cssEase: 'ease',
            fade: true,
            arrows: false,
            dots: true,
            infinite: true,
            speed: 500,
            pauseOnHover: false,
            autoplay: false,
            draggable: false,
            autoplaySpeed: 100,
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnFocus: false,
            pauseOnDotsHover: true,
            slide: '.slick-slide', // Cause trouble with responsive settings
        });
        $('#home-slider').on('afterChange', function(event, slick, currentSlide) {
            if (currentSlide == 0) {

                $('#home-slider').slick('slickPause');
            }
            if (currentSlide > 0) {
                $('#home-slider').slick("setOption", "autoplaySpeed", 10000);
            }
        });
        $('#home-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            if (nextSlide == 0) {
                theVideo.play();
                $('#home-slider').slick("setOption", "autoplaySpeed", 100);
            }
        });
        document.getElementById('theVideo');
        /*function myHandler(e) {
        	$('#home-slider').slick('slickPlay');
        }*/
        var $video_tag = $('#home-slider').find('video');
        if ($video_tag.length > 0) {
            var video = $('#home-slider .slick-active').find('video').get(0).play();
        }
        $video_tag.bind("ended", function() {
            $('#home-slider').slick('slickPlay');
        });

        // $('#home-slider .slick-slide').find('video').get(0).pause();
        /*var $video_tag = $('#home-slider').find('video');
		if($video_tag.length >0){
			var video = $('#home-slider .slick-active').find('video').get(0).play();
		}

		$video_tag.bind("ended", function() {
		   	$video_tag.fadeOut();
		});
		$('.first-slide-link').click(function(){
  			$video_tag.fadeIn();
			$('#home-slider .slick-active').find('video').get(0).play();
		});*/
        // if($(window).width() => 641){


        $('.designed-featured-container').each(function() {
            if ($(window).width() > 970) {
                if ($(window).width() > 1600) {
                    var $startWidth = 255;
                } else if ($(window).width() > 1501) {
                    var $startWidth = 220;
                } else if ($(window).width() > 1401) {
                    var $startWidth = 210;
                } else if ($(window).width() > 1301) {
                    var $startWidth = 200;
                } else if ($(window).width() > 970) {
                    var $startWidth = 170;
                }



                $(this).find('li span').each(function() {
                    $(this).css('width', $startWidth);
                    $startWidth += 10;
                });
            }
        });
        // }
        //Remove placeholder on click
        $("input,textarea").each(function() {
            $(this).data('holder', $(this).attr('placeholder'));

            $(this).focusin(function() {
                $(this).attr('placeholder', '');
            });

            $(this).focusout(function() {
                $(this).attr('placeholder', $(this).data('holder'));
            });
        });

        //Make elements equal height
        $('.matchHeight').matchHeight();

        $('.menu-icon-container').matchHeight();



        // Add fancybox to images
        $('.gallery-item a').attr('rel', 'gallery');
        $('a[rel*="album"], .gallery-item a, .fancybox, a[href$="jpg"], a[href$="png"], a[href$="gif"]').fancybox({
            minHeight: 0,
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        var vid = document.getElementsByClassName('mobile_slider_video')[0];
        if (vid != undefined) { addListeners(vid) };
        var anim = document.getElementsByClassName('home_page_mobile_slide');
        var anim_cont = document.getElementById('home_page_mobile_slider');

        if (anim_cont != undefined) { addListenersForMobileSliderAnimation(anim, anim_cont) };

        function addListenersForMobileSliderAnimation(anim, anim_cont) {
            anim[7].addEventListener("animationstart", function(evt) {
                console.log('anim-start');
            }, false);
            anim[7].addEventListener("animationend", function(evt) {
                evt.preventDefault;
                console.log('anim-end');
                //vid.classList.add("mobile_slider_video_anim");
                vid.classList.remove("mobile_slider_video_anim_end");
                vid.classList.add("mobile_slider_video_anim_start");
                vid.play();
                anim_cont.classList.remove("home_page_mobile_slider_anim");
            }, false);
        }

        function addListeners(vid) {
            vid.addEventListener('playing', function(evt) {
                console.log('vid-playing');
            }, false);
            vid.addEventListener('ended', function(evt) {
                evt.preventDefault;
                console.log('vid-ended');
                vid.classList.remove("mobile_slider_video_anim_start");
                vid.classList.add("mobile_slider_video_anim_end");
                anim_cont.classList.add("home_page_mobile_slider_anim");
                //void anim_cont.offsetWidth;
            }, false);
        }

        // Sticky footer
        bumpIt();


    });


    // Scripts which runs after all elements load

    $(window).load(function() {
        $('.top-bar .submenu > li a').matchHeight();
        $("body").fadeIn("fast");


        //jQuery code goes here


    });

    // Scripts which runs at window resize

    $(window).resize(function() {

        //jQuery code goes here


    });





}(jQuery));