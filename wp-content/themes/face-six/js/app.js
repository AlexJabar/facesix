;

var activeSlug;
var activePeriod;
var var_price;
var var_id;
var qnt;

(function ($) {
    $(document).ready(function () {

        $('.woocommerce-variation').remove(); // ???

        $(".woocommerce nav.woocommerce-MyAccount-navigation [href]").each(function () { // need to be in dashboard script
            if (this.href == window.location.href) {
                $(this).addClass("menu_acive_button");
            }
        });

        selectPlan($(".plan-name-box:first"));                       // selected attributes on load
        selectPeriod($(".switch-toggle.switch-candy input:first"));      //

        $(".plan-name-box").click(function () { // period switcher
            selectPlan($(this));

        });

        $(".switch-toggle.switch-candy input").click(function () { // period switcher
            selectPeriod($(this));
        });

        $("#plan-first-continue").click(function () {
            $('.columns.small-12.shop-plan-variation-container').fadeOut(300);
            $('.columns.small-12.shop-loader-container').delay(300).fadeIn().delay(500).fadeOut();
            $('.columns.small-12.shop-options-container').delay(1700).fadeIn();
        });

        $('.variations_form').each(function () { // change price on variation select

            $(this).on('found_variation', function (event, variation) {
                var_price = variation.display_price;
                var_id    = variation.variation_id;
                changePrice();
            });
        });


        $('input.qty').on('keyup', function () { // change price on quantity select
            changePrice();
            showAttrs();
        });

        $("button.shop-quantity-inc-button").click(function () { // inc qnt button
            $('input.qty').attr('value', ++qnt);
            changePrice();
            showAttrs();
        });

        $("button.shop-quantity-dec-button").click(function () { // dec qnt button
            $('input.qty').attr('value', --qnt);
            changePrice();
            showAttrs();
        });


    });

    function selectPlan(plan) {
        $(".plan-name-box").css("background-color", "#da9895").removeClass("currently-active");
        $(".arrow-down").css("display", "none");
        plan.css("background-color", "#b41c17").addClass("currently-active");
        activeSlug = plan.attr('id');
        activeSlug = activeSlug.replace('plan-name-id-', '');

        $(".app-label-span").html($("h1.page-title").text()); // output subscr details
        $(".plan-label-span").html(activeSlug.charAt(0).toUpperCase() + activeSlug.slice(1));

        $(".active-shop-variation").each(function () {
            $(this).removeClass("active-shop-variation");
        });

        $(".plan-group-" + activeSlug).each(function () {
            $(this).addClass("active-shop-variation");

        });

        $(".arrow-down").each(function () {
            if ($(this).hasClass("active-shop-variation")) {
                $(this).css("display", "block");
            }
        });

        $("table.variations select#pa_plan").val(activeSlug).trigger('change');
        $(".shop-info-product-thumbnail-plan").text(activeSlug.charAt(0).toUpperCase() + activeSlug.slice(1));

        showAttrs();
    }

    function selectPeriod(period) {
        activePeriod = period.attr('id');
        $("table.variations select#pa_period").val(activePeriod).trigger('change');

        if (activePeriod === 'year') {
            $(".toggle-discount-info").css("opacity","0");
        } else $(".toggle-discount-info").css("opacity","1");

    }

    function changePrice() {

        qnt = $("input.qty").val();
        if (qnt <= 0 ) {                         // cameras amount validation
            alert('Minimum 1 camera allowed');
            $("input.qty").val(1);
            qnt = 1;
        }

        var base_price = var_price * qnt;
        var fee;
        $(".shop-variation-dynamic-price-text.price-output-area").html(base_price + " $");
        $(".price-per-camera-amount").html(var_price+ " $");

        var discount;
        var j = 0;
        for (var k in disc_data){
            if (typeof disc_data[k] !== 'function') {

               if ( parseInt(qnt) >=  k) {
                  j++;
               }
            }
        }

        if (j === 0) {
            discount = 0;
        } else {
            var disc_val = Object.values(disc_data);
            discount = disc_val[j-1]/100;
        }

        if (discount > 0) {
            var discount_amt = Math.round(((var_price * qnt) - (var_price * qnt * (1 - discount))) * 100) / 100;

            $(".shop-variation-dynamic-price-text.text-discount-output-area").html('Discount on Cameras Amount');
            $(".shop-variation-dynamic-price-text.discount-output-area").html("- " + discount_amt + " $");

        } else {
            discount_amt = 0;
            $(".shop-variation-dynamic-price-text.text-discount-output-area").html('');
            $(".shop-variation-dynamic-price-text.discount-output-area").html('');
        }

        var total_amt = base_price + parseInt(fees[var_id]) - discount_amt;
        console.log(fees);
        if (fees[var_id] > 0) {
            $(".shop-variation-dynamic-price-fee-container .shop-variation-dynamic-price-text").html("Sign up fee");
            $(".shop-variation-dynamic-price-text.fee-output-area").html(fees[var_id] + " $");
        }
            $(".shop-variation-dynamic-price-text.total-output-area").html(total_amt + " $");

    }


    function showAttrs() {  // info section on 2nd subscribe page
        var attr_key;
        var attr_val;
        $(".shop-info-attr-container").remove();
        $(".shop-plan-variation-body").find(".active-shop-variation").each(function () {
            attr_key = $(this).parent().siblings(".shop-plan-variation-customfields-titles-row").text();
            attr_val = $(this).html();
            if (attr_key.trim() === 'No. Of Visits') {
                attr_val = $(this).html() *  $("input.qty").val();
            }

            $(".shop-info-main-attrs").append("<div class='shop-info-attr-container'>" +
                "<div class = 'shop-info-attr-key'>" +attr_key + "</div>" +
                "<div class = 'shop-info-attr-val'>" +attr_val + "</div>" +
                "</div>");
        });
    }
}(jQuery));




