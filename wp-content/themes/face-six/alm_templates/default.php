<div class="large-4 medium-6 small-12 columns blog-post-col matchHeight">
	<?php
		$cat_arg = array('hide_empty'=> true, 'exclude'=>array(1,9,10));
		$main_post_cats = get_categories($cat_arg);
	?>
	<!--<div class="post-data">
		<p class='text-center'>
			<span>
				<?php foreach($main_post_cats as $post_cat): ?>
					<?php echo $post_cat->name . ' ';  ?>
				<?php endforeach; ?>
			</span>
			<span>
				<?php echo  get_the_date('d.j.Y '); ?>
			</span>
		</p>
	</div> -->
	<h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
	<p><?php the_excerpt(); ?></p>
	<!--<div class="post-author-comment-container">
		<p class="post-author text-left">by <?php the_author(); ?></p>
		<p class="post-comments-quantity"><?php comments_number();  ?></p>
	</div>-->
</div>