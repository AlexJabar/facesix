<?php
/**
 * Template Name: Home Page
 */
get_header(); ?>

	<!--HOME PAGE SLIDER-->
<?php if(!wp_is_mobile()): ?>
	<?php echo home_slider_template(); ?>
<?php endif; ?>
	<!--END of HOME PAGE SLIDER-->
<?php if(wp_is_mobile()): ?>
<div id="home_page_mobile_slider" class="">
	<a href="https://www.face-six.com/patient-identification/" class="home_page_mobile_slide"><img class=''  src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner8_m-min.jpg" alt="" /></a>
	<a href="https://www.face-six.com/drone/" class="home_page_mobile_slide"><img class=''  src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner7_m-min.jpg" alt="" /></a>
	<a href="https://www.face-six.com/get-alert-faces-door/" class="home_page_mobile_slide"><img class=''  src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner61_m-min.jpg" alt="" /></a>
	<a href="https://www.face-six.com/time-attendance/" class="home_page_mobile_slide"><img class=''  src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner6_m-min.jpg" alt="" /></a>
	<a href="https://www.face-six.com/event-attendance/" class="home_page_mobile_slide"><img class=''  src="http://www.face-six.com/wp-content/uploads/2018/05/face-six_banner5_m-min.jpg" alt="" /></a>
	<a href="https://www.face-six.com/classroom-attendance-reinvented/" class="home_page_mobile_slide"><img class=''  src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner4_m-min-1.jpg" alt="" /></a>
	<a href="https://www.face-six.com/retail-demographic-made-easy/" class="home_page_mobile_slide"><img  class='' src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner3_m-min.jpg" alt="" /></a>
	<a href="https://www.face-six.com/face-recognition-security-alerts/" class="home_page_mobile_slide"><img class=''  src="https://www.face-six.com/wp-content/uploads/2018/05/face-six_banner2_m-min.jpg" alt="" /></a>
	<video id="" playsinline autoplay muted style="" poster="https://www.face-six.com/wp-content/uploads/2018/04/mobile_poster.gif" class="mobile_slider_video mobile_slider_video_anim_start">
		<source src="https://www.face-six.com/wp-content/uploads/2018/04/FA6_9.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' media="all">
	</video>
</div>
<?php endif; ?>
	<!-- BEGIN of main content -->
<div class="what_is_face_fix_soft">
		<div class="row">
			<?php if($what_is_face_fix_soft_content = get_field('what_is_face_fix_soft_content')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight">
					<div class="how-content">
						<?php echo $what_is_face_fix_soft_content ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if($what_is_face_fix_soft_section_background_image = get_field('what_is_face_fix_soft_section_background_image')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight what-image-col" <?php bg($what_is_face_fix_soft_section_background_image, 'large'); ?>></div>
			<?php endif; ?>

		</div>
	</div>
<?php if(have_rows('fa6-applications')): ?>
		<div class="fa6-applications-section">
			<?php $i = 1; ?>
			<div class="row">
			<?php $application_title = get_field('application_title'); ?>
			<?php $application_description = get_field('application_description'); ?>
			<div class="row application-title">
				<?php if($application_title): ?>
					<h2 class="text-center"><?php echo $application_title ; ?></h2>
				<?php endif; ?>
			</div>
			<div class="row application-description">
				<?php if($application_description): ?>
					<p class="text-center"><?php echo $application_description ; ?></p>
				<?php endif; ?>
			</div>
				<?php while(have_rows('fa6-applications')): the_row();
					$icon = get_sub_field('icon_fa6-applications');
					$title = get_sub_field('title_fa6-applications');
					$description = get_sub_field('description_fa6-applications');
				?>
					<div class="large-3 medium-6 small-12 columns">
						<div class="fa6-applications-item">
							<div class="fa6-applications-item-content">
							<div class="fa6-applications-item-img">
								<img src="<?php echo $icon['sizes']['medium'] ?>" alt="">
							</div>
								<!--<h2><?php echo $title; ?></h2>-->
								<p><?php echo $description ; ?></p>
							</div>
						</div>
					</div>
					<?php if($i % 4 == 0): ?>
						</div><div class="row">
					<?php endif; ?>
					<?php $i ++; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
<div class="left-article">
		<div class="row">
			<?php if($left_article_content = get_field('left_article_content')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight">
					<div class="how-content">
						<?php echo $left_article_content?>
					</div>
				</div>
			<?php endif; ?>
			<?php if($left_article_background_image = get_field('left_article_background_image')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight left-image-col" <?php bg($left_article_background_image, 'large'); ?>></div>
			<?php endif; ?>

		</div>
	</div>
<div class="right-article">
		<div class="row">
			<?php if($right_article_background_image = get_field('right_article_background_image')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight right-image-col" <?php bg($right_article_background_image, 'large'); ?>></div>
			<?php endif; ?>
			<?php if($right_article_content = get_field('right_article_content')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight">
					<div class="how-content">
						<?php echo $right_article_content?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<div class="left-article">
		<div class="row">
			<?php if($left_article_content2 = get_field('left_article_content2')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight">
					<div class="how-content">
						<?php echo $left_article_content2?>
					</div>
				</div>
			<?php endif; ?>
			<?php if($left_article_background_image2 = get_field('left_article_background_image2')): ?>
				<div class="large-6 medium-6 small-12 columns matchHeight left-image-col" <?php bg($left_article_background_image2, 'large'); ?>></div>
			<?php endif; ?>

		</div>
	</div>
	<!--<?php if(have_rows('half_width_sections')): ?>
		<div class="half-width-section">
			<div class="row">
				<?php while(have_rows('half_width_sections')): the_row();
					$half_width_section_content = get_sub_field('half_width_section_content');
				?>
					<div class="large-6 medium-6 small-12 columns">
						<div class="half-width-content">
							<?php echo $half_width_section_content ; ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>-->
	<!-- END of main content -->
	<div class="how-it-works-section">
		<div class="row expanded">
			<?php if($how_it_works_section_content = get_field('how_it_works_section_content')): ?>
				<div class="large-9 medium-6 small-12 columns matchHeight">
					<div class="how-content">
						<?php echo $how_it_works_section_content ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if($how_it_works_section_background_image = get_field('how_it_works_section_background_image')): ?>
				<div class="large-3 medium-6 small-0 columns matchHeight how-image-col" <?php bg($how_it_works_section_background_image, 'large'); ?>></div>
			<?php endif; ?>

		</div>
	</div>
<?php
		$advantages_title = get_field('advantages_title');
		$advantages_list = get_field('advantages_list');
		$advantages_image = get_field('advantages_image');
	?>
	<?php if($advantages_title || $advantages_list || $advantages_image): ?>
		<div class="advantages-section">
			<div class="row advantages-title-row columns">
				<?php if($advantages_title): ?>
					<h2 class="text-center"><?php echo $advantages_title ; ?></h2>
				<?php endif; ?>
			</div>
			<div class="row">
				<?php if($advantages_list): ?>
					<div class="columns small-12 <?php echo $advantages_image ? 'large-6' : '' ?> advantages-list-col">
						<?php if(have_rows('advantages_list')): ?>
							<ul class="advantages-list">
								<?php while(have_rows('advantages_list')): the_row();
										$item = get_sub_field('featured_item');
								?>
									<li><img class="adv-check-sign" src=<?= get_site_url(). "/wp-content/uploads/2018/02/check-mark.png"?>><h2 class="text-center"><?php echo $item; ?></h2></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if($advantages_image): ?>
					<div class="columns small-12 <?php echo $advantages_list ? 'large-6' : '' ; ?>">
						<div class="advantages-img text-center">
							<img src="<?php echo $advantages_image['sizes']['large'] ?>" alt="">
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
<?php if($logos_gallery = get_field('logos_gallery','options')): ?>
	<!--<div class="footer-logos-section">
		<div class="row">
			<?php foreach($logos_gallery as $logo): ?>
				<div class="large-3 medium-3 small-6 columns text-center matchHeight">
					<img src="<?php echo $logo['sizes']['medium'] ?>" alt="">
				</div>
			<?php endforeach; ?>
		</div>
	</div>-->
<?php endif; ?>
<?php get_footer(); ?>