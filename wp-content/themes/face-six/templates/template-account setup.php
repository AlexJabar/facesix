<?php
/**
 * Template Name: Account Settings  Page
 */

if (isset($_COOKIE['redirect_to_order_received'])) {
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
    //echo $protocol;
    wp_redirect( $protocol.$_COOKIE['redirect_to_order_received'] ); 
} 

get_header();
?>

<div class="default-page <?php echo $page_top_banner ? '' : 'without-banner' ?>"> 
  <div class="row large-row">

<?php
    
if(is_user_logged_in()){
    $cur_user_id = get_current_user_id();
    //print_r(get_user_meta($cur_user_id));
    $user = get_userdata($cur_user_id);
    $hash = $user->data->user_pass;
    $mail = get_userdata($cur_user_id)->user_email;
    
    $f6Id = get_user_meta($cur_user_id, 'f6Id', true);
    $f6UserId = get_user_meta($cur_user_id, 'f6UserId', true);
    $token = $_SESSION['token']; 
    $expirityDate =  $_SESSION['expirityDate'];
 
    if(isset($_POST) && $_POST != ""){
        
      
        $oldpassword = htmlspecialchars($_POST['oldpass']);
        $new_password = htmlspecialchars($_POST['newpass']);

        if(isset($oldpassword) && ($oldpassword != '')){

            if ( wp_check_password($oldpassword, $hash ) ){
                if(isset($new_password) && ($new_password != '')){
                        if(isset($token) && $token != '' && isset($expirityDate) && $now < $expirityDate){
                            changePassword($f6UserId, $token, $mail, $new_password);
                            wp_set_password( $new_password, $cur_user_id );
                        }
                        else{
                            wp_set_password( $new_password, $cur_user_id );
                        }     
                }
            }
            else
                echo "Your old password is wrong";     
        }

        $changemail = htmlspecialchars($_POST['changemail']);
        $confpass = htmlspecialchars($_POST['confpass']);
        if(isset($changemail) && ($changemail != "") && isset ($confpass) && ($confpass != "")){
            if(wp_check_password($confpass, $hash)){
                if(isset($token) && $token != '' && isset($expirityDate) && $now < $expirityDate){
                    changePassword($f6UserId, $token, $changemail, $confpass);
                    update_user_meta($cur_user_id, 'billing_email',  $changemail);
                    $args = array(
                        'ID'         => $cur_user_id,
                        'user_email' => $changemail
                    );
                    wp_update_user($args);
                    $wpdb->update($wpdb->users, array('user_login' => $changemail), array('ID' => $cur_user_id));
                 
                    
                }
                else{
                    $args = array(
                        'ID'         => $cur_user_id,
                        'user_email' => $changemail
                    );
                    update_user_meta($cur_user_id, 'billing_email',  $changemail);
                    wp_update_user($args);
                    $wpdb->update($wpdb->users, array('user_login' => $changemail), array('ID' => $cur_user_id));
                   
                    
                }
            }
            else{
                echo "Your password is wrong";  
            }
        }
        unset($_POST);
    }
    
  
    ?>
    <div class='userdash'>
        <form method="post" action = "<?php echo get_site_url() . '/account-settings'; ?>">
            <p> Old Password: <input type = 'password' name = "oldpass" placeholder = "********"/> </p> 
            <p> New Password: <input type = 'password' name = "newpass" placeholder = "********"/> </p> 
            <input type = "submit" value = 'Update Profile'/>
        </form>
    </div class='userdash'>

    <hr>
    <div class='userdash'>
        <form method="post" action = "<?php echo get_site_url() . '/account-settings'; ?>">
            <p> Mail: <input type = 'text' name = "changemail" placeholder = '<?php echo $mail;?>'/> </p>
            <p> Confirm Password: <input type = 'password' name = "confpass" placeholder = "********"/> </p> 
            <input type = "submit" value = 'Change mail'/>
        </form>
    </div class='userdash'>
       
    
<?php
} else {
    echo "Please loggin at first";
}
?>
</div>
</div>

<?php get_footer(); ?>