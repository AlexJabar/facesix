<?php
/**
 * Template Name: About Page
 */
get_header(); ?>
<?php $about_top_banner = get_field('about_top_banner') ?>
<div class="default-page <?php echo $about_top_banner ? '' : 'without-banner' ?> about-page">
	<?php if($about_top_banner): ?>
		<div class="top-banner banner-app" <?php bg($about_top_banner, 'full_hd') ?>></div>
	<?php endif; ?>

	<div class="row large-row">
		<!-- BEGIN of page content -->
		<div class="small-12 columns">
			<main class="main-content main-about">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<!-- <h1 class="page-title text-center"><?php //the_title(); ?></h1> -->
						<article <?php post_class(); ?>>
							<?php the_content('',true); ?>
						</article>
					<?php endwhile; ?>
				<?php endif; ?>
			</main>
		</div>
		<!-- END of page content -->
	</div>
	<section class="managment-section">
		<?php if($managment_title = get_field('managment_title')): ?>
			<div class="row columns title-row">
				<h1 class="text-center"><?php echo $managment_title ; ?></h1>
			</div>
		<?php endif; ?>
		<?php if( have_rows('managment_team') ): ?>
			<?php while( have_rows('managment_team') ): the_row();
				$team_member_photo = get_sub_field('team_member_photo');
				$team_member_name = get_sub_field('team_member_name');
				$team_member_position = get_sub_field('team_member_position');
				$team_member_bio = get_sub_field('team_member_bio');
			?>
			<div class="row team-member-row">
				<div class="large-4 medium-5 small-12 columns">
					<div class="team-member-photo-container">
						<img src="<?php echo $team_member_photo['sizes']['large'] ?>">
					</div>
				</div>
				<div class="large-8 medium-7 small-12 columns">
					<div class="team-member-info">
						<h2><?php echo $team_member_name ; ?></h2>
						<h6><?php echo $team_member_position ?></h6>
						<?php echo $team_member_bio ?>
					</div>
				</div>
			</div>
			<?php endwhile; ?>
		<?php endif; ?>
		<div class="row bottom-row"></div>
	</section>
	<div class="about-bottom-section">
		<?php if($bottom_content = get_field('bottom_content')): ?>
			<div class="row columns">
				<?php echo  $bottom_content ?>
			</div>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>