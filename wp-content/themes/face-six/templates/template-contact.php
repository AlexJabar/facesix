<?php

/**

 * Template Name: Contact Page

 */

get_header(); ?>

<?php $page_top_banner = get_field('contact_page_top_banner') ?>

<div class="default-page <?php echo $page_top_banner ? '' : 'without-banner' ?> contact-page">

	<?php if($page_top_banner): ?>

		<div class="top-banner banner-app" <?php bg($page_top_banner, 'full_hd') ?>>

		</div>

	<?php endif; ?>

	<div class="row large-row">

		<!-- BEGIN of page content -->

		<div class="small-12 columns">

			<main class="main-content">

				<?php if (have_posts()) : ?>

					<?php while (have_posts()) : the_post(); ?>

						<!-- <h1 class="page-title text-center"><?php //the_title(); ?></h1> -->

						<article <?php post_class(); ?>>

							<?php the_content('',true); ?>

						</article>

					<?php endwhile; ?>

				<?php endif; ?>

			</main>

		</div>



	</div>

	<div class="contact-two-columns-section">

		<div class="row">

			<?php

			$right_content_column = get_field('right_column_content');

			$left_content_column = get_field('left_column_content');

		 ?>



			<div class="small-12 columns large-6 medium-6">
<?php echo do_shortcode( '[contact-form-7 id="1099" title="Contact form 1"]') ?>


			</div>





			<div class="small-12 columns<?php echo $left_content_column ? ' large-4 medium-4' : '' ; ?>">
				<?php if($left_content_column): ?>
				<div class="column-content left-column-content">


					<?php echo $left_content_column ?>

				</div>
				<?php endif; ?>
				<?php if($right_content_column): ?>
				<div class="column-content right-column-content">

					<?php echo $right_content_column ?>

				</div>
				<?php endif; ?>

			</div>



		</div>

	</div>

</div>



<?php get_footer(); ?>