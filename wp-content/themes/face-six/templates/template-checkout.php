<?php
/**
 * Template Name: User checkout
 */
if ( !is_user_logged_in() ) {
	setcookie("ref_to_login", $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], time()+3600, '/');
	wp_redirect( wp_login_url() ); 
	exit;
}
if (isset($_COOKIE['ref_to_login'])) {
    unset($_COOKIE['ref_to_login']);
} 
get_header(); ?>
	<?php $page_top_banner = get_field('page_top_banner') ?>
<div class="default-page <?php echo $page_top_banner ? '' : 'without-banner' ?>">
	<?php if($page_top_banner): ?>
		<div class="top-banner" <?php bg($page_top_banner, 'full_hd') ?>>
			<?php if($top_banner_title = get_field('page_top_banner_title')): ?>
				<h2 class="top-banner-title"><?php echo $top_banner_title ; ?></h2>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<div class="row large-row">
		<!-- BEGIN of page content -->
		<div class="small-12 columns">
			<main class="main-content">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<!-- <h1 class="page-title text-center"><?php //the_title(); ?></h1> -->
						<article <?php post_class(); ?>>
							<?php the_content('',true); ?>
						</article>
					<?php endwhile; ?>
				<?php endif; ?>
			</main>
		</div>
		<!-- END of page content -->
	</div>
	<?php if(have_rows('possibilites')): ?>
		<div class="possibilities-section">
			<div class="row large-row columns small-uncollapse medium-uncollapse">
				<div class="possibilities-wrapper text-center">
					<?php while(have_rows('possibilites')): the_row();
						$first_line_text = get_sub_field('first_line_text');
						$second_line_text = get_sub_field('second_line_text');
						$third_line_text = get_sub_field('third_line_text');
					?>
						<div class="possibility-item">
							<p class="possibility-first-line"><?php echo $first_line_text ?></p>
							<p class="possibility-second-line"><?php echo $second_line_text ?></p>
							<p class="possibility-third-line"><?php echo $third_line_text ?></p>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if(have_rows('where_can_be_used')): ?>
		<div class="where-can-be-used-section">
			<?php $i = 1; ?>
			<div class="row large-row">
				<?php while(have_rows('where_can_be_used')): the_row();
					$icon = get_sub_field('icon_where_can_be_used');
					$title = get_sub_field('title_where_can_be_used');
					$description = get_sub_field('description_where_can_be_used');
				?>
					<div class="large-6 medium-12 small-12 columns">
						<div class="can-be-used-item">
							<div class="can-be-used-item-img">
								<img src="<?php echo $icon['sizes']['medium'] ?>" alt="">
							</div>
							<div class="can-be-used-item-content">
								<h2><?php echo $title; ?></h2>
								<p><?php echo $description ; ?></p>
							</div>
						</div>
					</div>
					<?php if($i % 2 == 0): ?>
						</div><div class="row large-row">
					<?php endif; ?>
					<?php $i ++; ?>
				<?php endwhile; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php
		$main_features_title = get_field('main_features_title');
		$main_features_list = get_field('main_features_list');
		$main_features_image = get_field('main_features_image');
	?>
	<?php if($main_features_title || $main_features_list || $main_features_image): ?>
		<div class="main-features-section">
			<div class="row main-features-title-row columns">
				<?php if($main_features_title): ?>
					<h2 class="text-center"><?php echo $main_features_title ; ?></h2>
				<?php endif; ?>
			</div>
			<div class="row">
				<?php if($main_features_list): ?>
					<div class="columns small-12 <?php echo $main_features_image ? 'large-6' : '' ?> main-features-list-col">
						<?php if(have_rows('main_features_list')): ?>
							<ul class="main-features-list">
								<?php while(have_rows('main_features_list')): the_row();
										$item = get_sub_field('featured_item');
								?>
									<li><?php echo $item; ?></li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if($main_features_image): ?>
					<div class="columns small-12 <?php echo $main_features_list ? 'large-6' : '' ; ?>">
						<div class="main-featured-img text-center">
							<img src="<?php echo $main_features_image['sizes']['large'] ?>" alt="">
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</div>


<?php get_footer(); ?>