<?php
/**
 * Template Name: User Page
 */

// if (isset($_COOKIE['redirect_to_order_received'])) {
//     $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
//     //echo $protocol;
//     wp_redirect( $protocol.$_COOKIE['redirect_to_order_received'] ); 
// } 

get_header();
?>

<div class="default-page <?php echo $page_top_banner ? '' : 'without-banner' ?>"> 
  <div class="row large-row">

<?php
$cur_user_id = get_current_user_id();

$first_name = get_user_meta($cur_user_id, 'first_name', true);
$last_name = get_user_meta($cur_user_id, 'last_name', true);
$f6Id = get_user_meta($cur_user_id, 'f6Id', true);
$country = get_user_meta($cur_user_id, 'country',true);
$city = get_user_meta($cur_user_id, 'city',true);

//print_r(get_user_meta($cur_user_id));
if(is_user_logged_in()){

    if(function_exists ( 'getLimits' )){
        $token = $_SESSION['token']; 
        $expirityDate =  $_SESSION['expirityDate'];
        $newName = htmlspecialchars($_POST['newName']);
        if(isset($token) && $token != '' && isset($expirityDate) && $now < $expirityDate) {
            if(isset($newName) && ($newName != '')){
                changeAccauntName(authenticate(), $f6Id, $newName);
             //  echo $f6Id;
               unset ($newName);
            }
            echo "<div class='userdash'>";
           
            $limits = getLimits($token);
            $user =  getUser($token);
          
          //  print_r($user);
            $limits = $limits['reply'];
           

           // echo 'Accaunt name: ' . $user["reply"]['users']['0']['accountName'] . "<br/>";
            
            // echo "Hi " . $first_name . " " . $last_name . "<br/>";
            // echo "Your country: " . $country . "<br/>";
            // echo "Your city: " . $city . "<br/>";
            echo 'Tariff: ' . $limits['tariff'] . "<br/>";
            echo 'Start Time: ' . $limits['starttime'] . "<br/>";
            echo 'End Time: ' . $limits['endtime'] . "<br/>";
            echo 'Subjects: ' . $limits['currentsubjectcount'] . '/' . $limits['maxsubjects'] . "<br/>";
            echo 'Matches: ' . $limits['currentmatchcount'] . '/' . $limits['maxmatches'] . "<br/>";
            echo 'Cameras: ' . $limits['currentcameracount'] . '/' . $limits['maxcameras'] . "<br/>";
           
            echo "<form method = 'post'>";
            echo " Accaunt name:  <input type = 'text' name = 'newName' placeholder =  '" . $user["reply"]['users']['0']['accountName'] . " '/> ";
            echo "<input type = 'submit' value = 'Change accaunt name'> </br>"; 
            echo "</form>";
            echo "</div class='userdash'>";

        }else{
            echo "<div class='userdash'>";
                 echo "Hi " . $first_name . " " . $last_name;
             echo "</div class='userdash'>";
            }
    } 
    ?>
    <br />
    <a href="<?php echo wp_logout_url(); ?>">Logout</a> 
    <?php

} else {
    echo "Please loggin at first";
}
?>
</div>
</div>

<?php get_footer(); ?>
