<?php
/**
 * Template Name: UserInfo Page
 */

//if (isset($_COOKIE['redirect_to_order_received'])) {
    //$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
    //echo $protocol;
   // wp_redirect( $protocol.$_COOKIE['redirect_to_order_received'] ); 
//}

get_header();
?>

<div class="default-page <?php echo $page_top_banner ? '' : 'without-banner' ?>"> 
  <div class="row large-row">

<?php

    
if(is_user_logged_in()){
    $cur_user_id = get_current_user_id();
    //print_r(get_user_meta($cur_user_id));
    



 
    if(isset($_POST) && $_POST != ""){
        
        $new_first_name = htmlspecialchars($_POST['first_name']);
        $new_last_name = htmlspecialchars($_POST['last_name']);
        $new_phone = htmlspecialchars($_POST['phone']);
        $new_country = htmlspecialchars($_POST['billing_country']);
        $new_city = htmlspecialchars($_POST['city']);
        $new_street = htmlspecialchars($_POST['street']);
        $new_company = htmlspecialchars($_POST['company']);
        $oldpassword = htmlspecialchars($_POST['oldpass']);
        $new_password = htmlspecialchars($_POST['newpass']);

        if(isset($new_first_name) && ($new_first_name != '')){
            update_user_meta($cur_user_id, 'first_name',  $new_first_name);
            update_user_meta($cur_user_id, 'billing_first_name',  $new_first_name);
        }
        if(isset($new_last_name) && ($new_last_name != '')){
            update_user_meta($cur_user_id, 'last_name',  $new_last_name);
            update_user_meta($cur_user_id, 'billing_last_name',  $new_last_name);
        }
        // if(isset($new_phone) && ($new_phone != '')){
        //     update_user_meta($cur_user_id, 'phone',  $new_phone);
        //     update_user_meta($cur_user_id, 'billing_phone',  $new_phone);
            
        // }
        if (isset($new_phone) && ($new_phone != '')){
            $new_phone = preg_replace('/[^0-9]/', '', $new_phone);
                if(strlen($new_phone) < 10 || strlen($new_phone)>12) {
                    //$reg_errors->add( 'phone', 'Looks like phone number is not valid'); 
                echo "Phone error";
                }
                else{
                    update_user_meta($cur_user_id, 'phone',  $new_phone);
                    update_user_meta($cur_user_id, 'billing_phone',  $new_phone);
                }
        }
        if(isset($new_country) && ($new_country != '')){
            update_user_meta($cur_user_id, 'country',  $new_country);
            update_user_meta($cur_user_id, 'billing_country',  $new_country);
            
        
        }
        if(isset($new_city) && ($new_city != '')){
            update_user_meta($cur_user_id, 'city',  $new_city);
            update_user_meta($cur_user_id, 'billing_city',  $new_city);
        }
        if(isset($new_company) && ($new_company != '')){
            
            update_user_meta($cur_user_id, 'billing_company',  $new_company);
        }
        if(isset($new_street) && ($new_street != '')){
            update_user_meta($cur_user_id, 'billing_address_1',  $new_street);
        }
       
        // if(isset($oldpassword) && ($oldpassword != '')){
        //     $user = get_userdata($cur_user_id);
        //     $hash = $user->data->user_pass;
        //     $mail = get_userdata($cur_user_id)->user_email;

        //     if ( wp_check_password($oldpassword, $hash ) ){
        //         if(isset($new_password) && ($new_password != '')){
        //             $f6Id = get_user_meta($cur_user_id, 'f6Id', true);
        //             $f6UserId = get_user_meta($cur_user_id, 'f6UserId', true);
        //             $token = $_SESSION['token']; 
        //             $expirityDate =  $_SESSION['expirityDate'];
                   

        //             if(isset($token) && $token != '' && isset($expirityDate) && $now < $expirityDate){
        //                 changePassword($f6UserId, $token, $mail, $new_password);
        //                 wp_set_password( $new_password, $cur_user_id );
                        
        //             }
        //             else{
        //                 wp_set_password( $new_password, $cur_user_id );
        //             }
                    
        //         }
        //     }
        //     else
        //         echo "You old password is wrong";     
        // }
        unset($_POST);
    }
    

    $first_name = get_user_meta($cur_user_id, 'first_name', true);
    $last_name = get_user_meta($cur_user_id, 'last_name', true);
    $phone = get_user_meta($cur_user_id,'phone',true);
    $country = get_user_meta($cur_user_id, 'country',true);
    $city = get_user_meta($cur_user_id, 'city',true);
    $userData = get_userdata($cur_user_id);
    $mail = $userData->user_email;
    $street =  (get_user_meta($cur_user_id, 'billing_address_1',true));
    $billingcountry = get_user_meta($cur_user_id, 'billing_country', true);
    $company = get_user_meta($cur_user_id, 'billing_company',true);
  

    ?>
           <div class='userdash'>
                <form method="post" action = "<?php echo get_site_url() . '/user-info/'; ?>">
                <p> First name: <input type = 'text' name = "first_name" placeholder = "<?php echo $first_name; ?>"/> </p> 
                <p> Last name: <input type = 'text'name = "last_name" placeholder = "<?php echo $last_name; ?>"/> </p> 
                
                <p> Company: <input type = 'text' name = "company" placeholder = "<?php echo $company; ?>"/> </p> 
                <p> Phone: <input type = 'text' name = "phone" placeholder = "<?php echo $phone; ?>"/> </p> 
               
                <?php  
                   
                   $field = [
                        'type' => 'country',
                        'label' => 'Country',
                        'required' => 0,
                        'class' => ['address-field'],
                        
                ];
                woocommerce_form_field( 'billing_country', $field, $billingcountry ); 
               
                ?>
                <p> City: <input type = 'text' name = "city" placeholder = "<?php echo $city; ?>"/> </p> 
                <p> Street address: <input type = 'text' name = "street" placeholder = "<?php echo $street; ?>"/> </p> 
                <input type = "submit" value = 'Update Profile'/>
                </form>
            </div class='userdash'>
       
    
<?php
} else {
    echo "Please loggin at first";
}
?>
</div>
</div>

<?php get_footer(); ?>