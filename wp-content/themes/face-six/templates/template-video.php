<?php
/**
 * Template Name: Video Page
 */
get_header(); ?>
<?php $page_top_banner = get_field('video_page_top_banner') ?>
<div class="default-page videos-page <?php echo $page_top_banner ? '' : 'without-banner' ?>" >
	<?php if($page_top_banner): ?>
		<div class="top-banner banner-app" <?php bg($page_top_banner, 'full_hd') ?>>
		</div>
	<?php endif; ?>
	<!--<div class="row large-row">
		<div class="small-12 columns">
			<main class="main-content">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<article <?php post_class(); ?>>
							<?php the_content('',true); ?>
						</article>
					<?php endwhile; ?>
				<?php endif; ?>
			</main>
		</div>
	</div>-->
	<div class="videos-rows">
		<?php if( have_rows('videos') ): ?>
			<?php while( have_rows('videos') ): the_row();
				$title = get_sub_field('video_title');
				$text = get_sub_field('video_description');
				$video = get_sub_field('video');
		 	?>
				<div class="row row small-12 large-7 medium-9">
					<div class="columns"><h2 class="video-title"><?php echo $title ; ?></h2></div>
					<div class="columns">
						<?php echo iframe_wrapper($video) ?>
					</div>
					<div class="columns">
						<div class="columns video-desc"><?php echo $text ; ?></div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>