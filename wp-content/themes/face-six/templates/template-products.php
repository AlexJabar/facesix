<?php
/**
 * Template Name: Custom products
 */
get_header(); ?>
<?php $page_top_banner = get_field('page_top_bannerImage') ?>
<div class="default-page <?php echo $page_top_banner ? '' : 'without-banner' ?>">
	<?php if($page_top_banner): ?>
		<div class="top-banner" <?php bg($page_top_banner, 'full_hd') ?>>
		</div>
	<?php endif; ?>	
	<div class="row large-row">
		<!-- BEGIN of page content -->
		
			<!-- <?php
			// 	$products = wp_get_nav_menu_items( 'products-categories', [] ); 
			// 	echo '<div class="dropdown pull-right">';
			// 	echo '<ul class="vertical menu accordion-menu" data-accordion-menu>';
			// 	echo '<li>';
			// 	echo '<a href="#">Select another app</a>';
			// 	echo  '<ul style = display: none; class="menu vertical nested">';
			// 	foreach ($products as $item) {
			// 	$title = $item->title; 
			// 	$url = $item->url; 
			// 		$pr_menu_item = "<a href = ' " . $url . " '>" . $title . "</a>" . "<br>";
			// 	//	echo $pr_menu_item;
			// 		echo "<li>" . $pr_menu_item . "</li></li>";
					
			// }
			// 	echo "</ul>";
			// 	echo "</div>";

				
			?> -->

  
    
   
		<!-- <div class="small-3 columns">
			<?php //wp_nav_menu( array( 'menu' => 'products-categories')); ?>
		</div> -->
		<div class="small-12 columns">
			<main class="main-content prod-cont">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<!-- <h1 class="page-title text-center"><?php //the_title(); ?></h1> -->
						<article <?php post_class(); ?>>
							<?php the_content('',true); ?>
						</article>
					<?php endwhile; ?>
				<?php endif; ?>
				<h6 align="center">Choose another app</h6>
			</div>
			<div style="clear: both"></div>
			<div class="another_app">
					<?php
						
						$products = wp_get_nav_menu_items( 'products-categories', [] ); 
						echo '<pre>';
						//print_r($products);

						echo  '<ul class ="product_chosse">';
						foreach ($products as $item) {
						$image_id = get_field('dropdown_menu_icon', $item->ID, false );
						if(isset($image_id) && $image_id != ''){
							$title = wp_get_attachment_image( $image_id, 'full' ); 
						} else {
							$title = $item->title; 
						}
						$url = $item->url; 
						$currenturl = get_permalink();
						if($currenturl!=$url){
							$pr_menu_item = "<a href = ' " . $url . " '>" . $title . "</a>" . "<br>";
						
						//	echo $pr_menu_item;
							echo "<li class = 'liproduct_chosse'>" . $pr_menu_item . "</li>";
						}	
					}
						echo "</ul>";
						echo "</div>";
					?> 
			</main>
			
		</div>
			
	</div>
</div>

<?php get_footer(); ?>