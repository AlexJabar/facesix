<div class="login-form-container">
<?php if ( $attributes['show_title'] ) : ?>
    <h2><?php _e( 'Sign In', 'personalize-login' ); ?></h2>
<?php endif; ?>
 <!-- Show errors if there are any -->
<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
    <?php foreach ( $attributes['errors'] as $error ) : ?>
        <p class="login-error">
            <?php echo $error; ?>
        </p>
    <?php endforeach; ?>
<?php endif; ?>
<!-- Show logged out message if user just logged out -->
<?php if ( $attributes['logged_out'] ) : ?>
    <p class="login-info">
        <?php _e( 'You have signed out. Would you like to sign in again?', 'personalize-login' ); ?>
    </p>
<?php endif; ?>
<!-- Show registered message if user just registered -->
<?php if ( $attributes['registered'] ) : ?>
    <p class="login-info">
        <?php
            printf(
                __( 'You have successfully registered to <strong>%s</strong>. We have emailed your confirmation code and password to the email address you entered.', 'personalize-login' ),
                get_bloginfo( 'name' )
            );
        ?>
    </p>
<?php endif; ?>
<!-- Show lost password message if user reset pusword -->
<?php if ( $attributes['lost_password_sent'] ) : ?>
    <p class="login-info">
        <?php _e( 'Check your email for a link to reset your password.', 'personalize-login' ); ?>
    </p>
<?php endif; ?>
<?php if ( $attributes['password_updated'] ) : ?>
    <p class="login-info">
        <?php _e( 'Your password has been changed. You can sign in now.', 'personalize-login' ); ?>
    </p>
<?php endif; ?>
<?php if ( $attributes['confirmed'] ) : ?>
    <p class="login-info">
        <?php _e( 'Your registration email confirmed.', 'personalize-login' ); ?>
    </p>
<?php endif; ?>
<?php if ( $attributes['lost_confirmation'] ) : ?>
    <p class="login-info">
        <?php _e( 'Check your email for a link to confirm your registration', 'personalize-login' ); ?>
    </p>
<?php endif; ?>
<?php if ( $attributes['not_confirmed'] ) : ?>
    <p class="login-info">
        <?php _e( 'Please confirm your email first.', 'personalize-login' ); ?>
        <a class="forgot-password" href="<?php echo wp_lostpassword_url().'&request=confirmation_code'; ?>">
            <?php _e( 'Resend confirmation code', 'personalize-login' ); ?>
        </a>
    </p>
<?php endif; ?>
<?php
    // echo 'Sign In with Facebook';
    // echo do_shortcode('[fbl_login_button redirect="'.site_url().'/fb-login-checks/" hide_if_logged="" size="large" type="continue_with" show_face="true"]');
    // echo 'or fill the form';
?>
<?php 
    $redirect = $attributes['redirect'];
    if(isset($_COOKIE["ref_to_login"])){
        $redirect = $_COOKIE["ref_to_login"];
    }
 ?>
<?php
    wp_login_form(
        array(
            'label_username' => __( 'Email', 'personalize-login' ),
            'label_log_in' => __( 'Sign In', 'personalize-login' ),
            'redirect' => $redirect,
        )
    );
?>
<a class="new-registration" href="<?php echo wp_registration_url(); ?>">
    <?php _e( 'Not yet a registered user? Please first Register here', 'personalize-login' ); ?>
</a>
<br />
<a class="forgot-password" href="<?php echo wp_lostpassword_url(); ?>">
    <?php _e( 'Forgot your password?', 'personalize-login' ); ?>
</a>