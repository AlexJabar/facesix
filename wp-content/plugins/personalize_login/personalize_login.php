<?php
/**
 * Plugin Name:       Personalize Login
 * Description:       A plugin that replaces the WordPress login flow with a custom page.
 * Version:           1.0.0
 * Author:            YO
 * License:           GPL
 * Text Domain:       personalize-login
 */
 
class Personalize_Login_Plugin {
 
    /**
     * Initializes the plugin.
     *
     * To keep the initialization fast, only add filter and action
     * hooks in the constructor.
     */
    public function __construct() {
        
        add_shortcode( 'custom-login-form', array( $this, 'render_login_form' ) );

        add_action( 'login_form_login', array( $this, 'redirect_to_custom_login' ) );

        add_filter( 'authenticate', array( $this, 'maybe_redirect_at_authenticate' ), 101, 3 );

        add_action( 'wp_logout', array( $this, 'redirect_after_logout' ) );

        add_filter( 'login_redirect', array( $this, 'redirect_after_login' ), 10, 3 );

        add_shortcode( 'custom-register-form', array( $this, 'render_register_form' ) );

        add_action( 'login_form_register', array( $this, 'redirect_to_custom_register' ) );

        add_action( 'login_form_register', array( $this, 'do_register_user' ) );

        add_filter( 'admin_init' , array( $this, 'register_settings_fields' ) );

        add_action( 'wp_print_footer_scripts', array( $this, 'add_captcha_js_to_footer' ) );

        add_action( 'login_form_lostpassword', array( $this, 'redirect_to_custom_lostpassword' ) );

        add_shortcode( 'custom-password-lost-form', array( $this, 'render_password_lost_form' ) );

        add_action( 'login_form_lostpassword', array( $this, 'do_password_lost' ) );

        add_filter( 'retrieve_password_message', array( $this, 'replace_retrieve_password_message' ), 10, 4 );

        add_action( 'login_form_rp', array( $this, 'redirect_to_custom_password_reset' ) );
        add_action( 'login_form_resetpass', array( $this, 'redirect_to_custom_password_reset' ) );

        add_shortcode( 'custom-password-reset-form', array( $this, 'render_password_reset_form' ) );

        add_action( 'login_form_rp', array( $this, 'do_password_reset' ) );
        add_action( 'login_form_resetpass', array( $this, 'do_password_reset' ) );

        add_action('wp_authenticate', array( $this, 'is_email_confirmed' ), 30, 2);
            
    }


    /**
     * Plugin activation hook.
     *
     * Creates all WordPress pages needed by the plugin.
     */
    public static function plugin_activated() {
        // Information needed for creating the plugin's pages
        $page_definitions = array(
            'member-login' => array(
                'title' => __( 'Sign In', 'personalize-login' ),
                'content' => '[custom-login-form]'
            ),
            'member-account' => array(
                'title' => __( 'Your Account', 'personalize-login' ),
                'content' => '[account-info]'
            ),
            'member-register' => array(
                'title' => __( 'Register', 'personalize-login' ),
                'content' => '[custom-register-form]'
            ),
            'member-password-lost' => array(
                'title' => __( 'Forgot Your Password?', 'personalize-login' ),
                'content' => '[custom-password-lost-form]'
            ),
            'member-password-reset' => array(
                'title' => __( 'Pick a New Password', 'personalize-login' ),
                'content' => '[custom-password-reset-form]'
            )
        );
     
        foreach ( $page_definitions as $slug => $page ) {
            // Check that the page doesn't exist already
            $query = new WP_Query( 'pagename=' . $slug );
            if ( ! $query->have_posts() ) {
                // Add the page using the data from the array above
                wp_insert_post(
                    array(
                        'post_content'   => $page['content'],
                        'post_name'      => $slug,
                        'post_title'     => $page['title'],
                        'post_status'    => 'publish',
                        'post_type'      => 'page',
                        'ping_status'    => 'closed',
                        'comment_status' => 'closed',
                    )
                );
            }
        }
    }

        /**
         * A shortcode for rendering the login form.
         *
         * @param  array   $attributes  Shortcode attributes.
         * @param  string  $content     The text content for shortcode. Not used.
         *
         * @return string  The shortcode output
         */
        public function render_login_form( $attributes, $content = null ) {
            // Parse shortcode attributes
            $default_attributes = array( 'show_title' => false );
            $attributes = shortcode_atts( $default_attributes, $attributes );
            $show_title = $attributes['show_title'];
         
            if ( is_user_logged_in() ) {
                return __( 'You are already signed in.', 'personalize-login' );
            }
             
            // Pass the redirect parameter to the WordPress login functionality: by default,
            // don't specify a redirect, but if a valid redirect URL has been passed as
            // request parameter, use it.
            $attributes['redirect'] = '';
            if ( isset( $_REQUEST['redirect_to'] ) ) {
                $attributes['redirect'] = wp_validate_redirect( $_REQUEST['redirect_to'], $attributes['redirect'] );
            }

            // Error messages
            $errors = array();
            if ( isset( $_REQUEST['login'] ) ) {
                $error_codes = explode( ',', $_REQUEST['login'] );
             
                foreach ( $error_codes as $code ) {
                    $errors []= $this->get_error_message( $code );
                }
            }
            $attributes['errors'] = $errors;

            // Check if the user just registered
            $attributes['registered'] = isset( $_REQUEST['registered'] );
            // Check if the user just confirm email
            $attributes['confirmed'] = isset( $_REQUEST['email']) && $_REQUEST['email'] == 'confirmed' ;

            // Check if user just logged out
            $attributes['logged_out'] = isset( $_REQUEST['logged_out'] ) && $_REQUEST['logged_out'] == true;

            // Check if the user just requested a new password 
            $attributes['lost_password_sent'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'confirm';
            // Check if the user just requested a new confirmation 
            $attributes['lost_confirmation'] = isset( $_REQUEST['checkemail'] ) && $_REQUEST['checkemail'] == 'resended';

            // Check if user just updated password
            $attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';
            // Check if user not confirm his email
            $attributes['not_confirmed'] = isset( $_REQUEST['email']) && $_REQUEST['email'] == 'not_confirmed';

             
            // Render the login form using an external template
            return $this->get_template_html( 'login_form', $attributes );
        }   

        /**
         * Redirect the user to the custom login page instead of wp-login.php.
         */
        function redirect_to_custom_login() {
            if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
                $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
             
                if ( is_user_logged_in() ) {
                    $this->redirect_logged_in_user( $redirect_to );
                    exit;
                }
         
                // The rest are redirected to the login page
                $login_url = home_url( 'member-login' );
                if ( ! empty( $redirect_to ) ) {
                    $login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
                }
         
                wp_redirect( $login_url );
                exit;
            }
        }  

        /**
         * Redirects the user to the correct page depending on whether he / she
         * is an admin or not.
         *
         * @param string $redirect_to   An optional redirect_to URL for admin users
         */
        private function redirect_logged_in_user( $redirect_to = null ) {
            $user = wp_get_current_user();
            if ( user_can( $user, 'manage_options' ) ) {
                if ( $redirect_to ) {
                    wp_safe_redirect( $redirect_to );
                } else {
                    wp_redirect( admin_url() );
                }
            } else {
                wp_redirect( home_url( 'member-account' ) );
            }
        }   
            

    /**
     * Renders the contents of the given template to a string and returns it.
     *
     * @param string $template_name The name of the template to render (without .php)
     * @param array  $attributes    The PHP variables for the template
     *
     * @return string               The contents of the template.
     */
    private function get_template_html( $template_name, $attributes = null ) {
        if ( ! $attributes ) {
            $attributes = array();
        }
     
        ob_start();
     
        do_action( 'personalize_login_before_' . $template_name );
     
        require( 'templates/' . $template_name . '.php');
     
        do_action( 'personalize_login_after_' . $template_name );
     
        $html = ob_get_contents();
        ob_end_clean();
     
        return $html;
    }

    /**
     * Redirect the user after authentication if there were any errors.
     *
     * @param Wp_User|Wp_Error  $user       The signed in user, or the errors that have occurred during login.
     * @param string            $username   The user name used to log in.
     * @param string            $password   The password used to log in.
     *
     * @return Wp_User|Wp_Error The logged in user, or error information if there were errors.
     */
    function maybe_redirect_at_authenticate( $user, $username, $password ) {
        // Check if the earlier authenticate filter (most likely, 
        // the default WordPress authentication) functions have found errors
        if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
            if ( is_wp_error( $user ) ) {
                $error_codes = join( ',', $user->get_error_codes() );
     
                $login_url = home_url( 'member-login' );
                $login_url = add_query_arg( 'login', $error_codes, $login_url );
     
                wp_redirect( $login_url );
                exit;
            }
        }
     
        return $user;
    }

    /**
     * Finds and returns a matching error message for the given error code.
     *
     * @param string $error_code    The error code to look up.
     *
     * @return string               An error message.
     */
    private function get_error_message( $error_code ) {
        switch ( $error_code ) {
            case 'empty_username':
                return __( 'You do have an email address, right?', 'personalize-login' );
     
            case 'empty_password':
                return __( 'You need to enter a password to login.', 'personalize-login' );
     
            case 'invalid_username':
                return __(
                    "We don't have any users with that email address. Maybe you used a different one when signing up?",
                    'personalize-login'
                );
     
            case 'incorrect_password':
                $err = __(
                    "The password you entered wasn't quite right. <a href='%s'>Did you forget your password</a>?",
                    'personalize-login'
                );
                return sprintf( $err, wp_lostpassword_url() );
            case 'not_confirmed': 
                $err = __(
                    "Please confirm your email before Sign In. <a href='%s'>Resend confirmation, reset passwprd</a>?",
                    'personalize-login'
                );
                return sprintf( $err, wp_lostpassword_url() );              

            // Registration errors
 
            case 'email':
                return __( 'The email address you entered is not valid.', 'personalize-login' );
             
            case 'email_exists':
                return __( 'An account exists with this email address.', 'personalize-login' );
             
            case 'closed':
                return __( 'Registering new users is currently not allowed.', 'personalize-login' );

            case 'captcha':
                return __( 'The Google reCAPTCHA check failed. Are you a robot?', 'personalize-login' );
            
            case 'pwd_much':
                return __( 'Password doesn\'t match confirmation', 'personalize-login'  );
            
            case 'phone_validation':
                return __( 'Phone Number is not valid.', 'personalize-login' );

            // Lost password
 
            case 'empty_username':
                return __( 'You need to enter your email address to continue.', 'personalize-login' );
             
            case 'invalid_email':
            case 'invalidcombo':
                return __( 'There are no users registered with this email address.', 'personalize-login' );

            // Reset password
            case 'confirmed':
                return __( 'Your registration email confirmed ' );

            case 'expiredkey':
            case 'invalidkey':
                return __( 'The password reset link you used is not valid anymore.', 'personalize-login' );
             
            case 'password_reset_mismatch':
                return __( "The two passwords you entered don't match.", 'personalize-login' );
                 
            case 'password_reset_empty':
                return __( "Sorry, we don't accept empty passwords.", 'personalize-login' );
                             
            default:
                break;
        }
         
        return __( 'An unknown error occurred. Please try again later.', 'personalize-login' );
    }

    /**
     * Redirect to custom login page after the user has been logged out.
     */
    public function redirect_after_logout() {
        $redirect_url = home_url( 'member-login?logged_out=true' );
        wp_safe_redirect( $redirect_url );
        exit;
    }

    /**
     * Returns the URL to which the user should be redirected after the (successful) login.
     *
     * @param string           $redirect_to           The redirect destination URL.
     * @param string           $requested_redirect_to The requested redirect destination URL passed as a parameter.
     * @param WP_User|WP_Error $user                  WP_User object if login was successful, WP_Error object otherwise.
     *
     * @return string Redirect URL
     */
    public function redirect_after_login( $redirect_to, $requested_redirect_to, $user ) {
        $redirect_url = home_url();
     
        if ( ! isset( $user->ID ) ) {
            return $redirect_url;
        }
     
        if ( user_can( $user, 'manage_options' ) ) {
            // Use the redirect_to parameter if one is set, otherwise redirect to admin dashboard.
            if ( $requested_redirect_to == '' ) {
                $redirect_url = admin_url();
            } else {
                $redirect_url = $requested_redirect_to;
            }
        } else {
            // Use the redirect_to parameter if one is set, otherwise redirect to admin dashboard.
            if ( $requested_redirect_to == '' ) {
                $redirect_url = home_url( 'member-account' );
            } else {
                $redirect_url = $requested_redirect_to;
            }           
        }
     
        return wp_validate_redirect( $redirect_url, home_url() );
    }

    /**
     * A shortcode for rendering the new user registration form.
     *
     * @param  array   $attributes  Shortcode attributes.
     * @param  string  $content     The text content for shortcode. Not used.
     *
     * @return string  The shortcode output
     */
    public function render_register_form( $attributes, $content = null ) {
        // Parse shortcode attributes
        $default_attributes = array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );
     
        if ( is_user_logged_in() ) {
            return __( 'You are already signed in.', 'personalize-login' );
        } elseif ( ! get_option( 'users_can_register' ) ) {
            return __( 'Registering new users is currently not allowed.', 'personalize-login' );
        } else {
            // Retrieve possible errors from request parameters
            $attributes['errors'] = array();
            if ( isset( $_REQUEST['register-errors'] ) ) {
                $error_codes = explode( ',', $_REQUEST['register-errors'] );
             
                foreach ( $error_codes as $error_code ) {
                    $attributes['errors'] []= $this->get_error_message( $error_code );
                }
            }
            // Retrieve recaptcha key
            $attributes['recaptcha_site_key'] = get_option( 'personalize-login-recaptcha-site-key', null );
            return $this->get_template_html( 'register_form', $attributes );
        }
    }

    /**
     * Redirects the user to the custom registration page instead
     * of wp-login.php?action=register.
     */
    public function redirect_to_custom_register() {
        if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
            if ( is_user_logged_in() ) {
                $this->redirect_logged_in_user();
            } else {
                wp_redirect( home_url( 'member-register' ) );
            }
            exit;
        }
    }

    /**
     * Validates and then completes the new user signup process if all went well.
     *
     * @param string $email         The new user's email address
     * @param string $first_name    The new user's first name
     * @param string $last_name     The new user's last name
     *
     * @return int|WP_Error         The id of the user that was created, or error if failed.
     */

    function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    private function register_user( $email, $first_name, $last_name, $phone, $pwd, $cpwd ) {
        $errors = new WP_Error();
        
        // Email address is used as both username and email. It is also the only
        // parameter we need to validate
        if ( ! is_email( $email ) ) {
            $errors->add( 'email', $this->get_error_message( 'email' ) );
            return $errors;
        }
     
        if ( username_exists( $email ) || email_exists( $email ) ) {
            $errors->add( 'email_exists', $this->get_error_message( 'email_exists') );
            return $errors;
        }

        if ( ! empty( $phone ) ) {
            $phone = preg_replace('/[^0-9]/', '', $phone);
            if(strlen($phone) != 10) {
                //$reg_errors->add( 'phone', 'Looks like phone number is not valid'); 
                $errors->add( 'phone_validation', $this->get_error_message( 'phone_validation') );
                return $errors;             
            }
        }

        if ( $pwd != $cpwd ) {
            $errors->add( 'pwd_much', $this->get_error_message( 'pwd_much') );
            return $errors;
        }
     
        // Generate the password so that the subscriber will have to check email...
        //$password = wp_generate_password( 12, false );
       
        $password = $pwd;
         
        //Local information about the user from API Request
         $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".'82.81.95.78'); //Change to $_SERVER['REMOTE_ADDR']
         $country = (string)$xml->geoplugin_countryCode;
         $city = (string)$xml->geoplugin_city; 
         $region = (string)$xml->geoplugin_region;
            
         $user_data = array(
            'user_login'    => $email,
            'user_email'    => $email,
            'user_pass'     => $password,
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'nickname'      => $first_name,
        );
     
        $user_id = wp_insert_user( $user_data );
        
        update_user_meta($user_id, 'phone', $phone);
        update_user_meta($user_id,'ip-address',$_SERVER['REMOTE_ADDR']);
        update_user_meta($user_id, 'country', $country);
        update_user_meta($user_id, 'country', $country);
        update_user_meta($user_id, 'city', $city);
        update_user_meta($user_id, 'region', $region);
        $admin_token = authenticate();
        $accname = $email;
        $accountsId = accounts($admin_token,$accname);
        $newUser = createUser($accountsId,$email,$password,$admin_token);
        $f6UserId = $newUser['id'];
        $userToken = authenticateUser($email, $password);
       // $locId = location($userToken);
        //$cliId = clients($userToken);
        update_user_meta($user_id, 'f6Id', $accountsId); //Accaunt ID
        update_user_meta($user_id, 'f6UserId', $f6UserId); //User ID
      

        $this->custom_wp_new_user_notification( $user_id, $password );
        
        return $user_id;
    }

    /**
     * Handles the registration of a new user.
     *
     * Used through the action hook "login_form_register" activated on wp-login.php
     * when accessed through the registration action.
     */
    public function do_register_user() {
        if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
            $redirect_url = home_url( 'member-register' );
     
            if ( ! get_option( 'users_can_register' ) ) {
                // Registration closed, display error
                $redirect_url = add_query_arg( 'register-errors', 'closed', $redirect_url );
            } elseif ( ! $this->verify_recaptcha() ) {
                // Recaptcha check failed, display error
                $redirect_url = add_query_arg( 'register-errors', 'captcha', $redirect_url );
            } else {
                $email = $_POST['email'];
                $first_name = sanitize_text_field( $_POST['first_name'] );
                $last_name = sanitize_text_field( $_POST['last_name'] );
                $phone = sanitize_text_field( $_POST['phone'] );
                $pwd = sanitize_text_field( $_POST['pwd'] );
                $cpwd = sanitize_text_field( $_POST['cpwd'] );
     
                $result = $this->register_user( $email, $first_name, $last_name, $phone, $pwd, $cpwd );
     
                if ( is_wp_error( $result ) ) {
                    // Parse errors into a string and append as parameter to redirect
                    $errors = join( ',', $result->get_error_codes() );
                    $redirect_url = add_query_arg( 'register-errors', $errors, $redirect_url );
                } else {
                    // Success, redirect to login page.
                    $redirect_url = home_url( 'member-login' );
                    $redirect_url = add_query_arg( 'registered', $email, $redirect_url );
                }
            }
     
            wp_redirect( $redirect_url );
            exit;
        }
    }
     
    /**
     * Registers the settings fields needed by the plugin.
     */
    public function register_settings_fields() {
        // Create settings fields for the two keys used by reCAPTCHA
        register_setting( 'general', 'personalize-login-recaptcha-site-key' );
        register_setting( 'general', 'personalize-login-recaptcha-secret-key' );
     
        add_settings_field(
            'personalize-login-recaptcha-site-key',
            '<label for="personalize-login-recaptcha-site-key">' . __( 'reCAPTCHA site key' , 'personalize-login' ) . '</label>',
            array( $this, 'render_recaptcha_site_key_field' ),
            'general'
        );
     
        add_settings_field(
            'personalize-login-recaptcha-secret-key',
            '<label for="personalize-login-recaptcha-secret-key">' . __( 'reCAPTCHA secret key' , 'personalize-login' ) . '</label>',
            array( $this, 'render_recaptcha_secret_key_field' ),
            'general'
        );
    }
     
    public function render_recaptcha_site_key_field() {
        $value = get_option( 'personalize-login-recaptcha-site-key', '' );
        echo '<input type="text" id="personalize-login-recaptcha-site-key" name="personalize-login-recaptcha-site-key" value="' . esc_attr( $value ) . '" />';
    }
     
    public function render_recaptcha_secret_key_field() {
        $value = get_option( 'personalize-login-recaptcha-secret-key', '' );
        echo '<input type="text" id="personalize-login-recaptcha-secret-key" name="personalize-login-recaptcha-secret-key" value="' . esc_attr( $value ) . '" />';
    }

    /**
     * An action function used to include the reCAPTCHA JavaScript file
     * at the end of the page.
     */
    public function add_captcha_js_to_footer() {
        echo "<script src='https://www.google.com/recaptcha/api.js'></script>";
    }

    /**
     * Checks that the reCAPTCHA parameter sent with the registration
     * request is valid.
     *
     * @return bool True if the CAPTCHA is OK, otherwise false.
     */
    private function verify_recaptcha() {
        // This field is set by the recaptcha widget if check is successful
        if ( isset ( $_POST['g-recaptcha-response'] ) ) {
            $captcha_response = $_POST['g-recaptcha-response'];
        } else {
            return false;
        }
     
        // Verify the captcha response from Google
        $response = wp_remote_post(
            'https://www.google.com/recaptcha/api/siteverify',
            array(
                'body' => array(
                    'secret' => get_option( 'personalize-login-recaptcha-secret-key' ),
                    'response' => $captcha_response
                )
            )
        );
     
        $success = false;
        if ( $response && is_array( $response ) ) {
            $decoded_response = json_decode( $response['body'] );
            $success = $decoded_response->success;
        }
     
        return $success;
    }

    /**
     * Redirects the user to the custom "Forgot your password?" page instead of
     * wp-login.php?action=lostpassword.
     */
    public function redirect_to_custom_lostpassword() {
        if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {

            if ( is_user_logged_in() ) {
                $this->redirect_logged_in_user();
                exit;
            }
            if ('confirmation_code' == $_GET['request']){
                wp_redirect( home_url( 'member-password-lost?request=confirmation_code' ) );
                exit;               
            }
     
            wp_redirect( home_url( 'member-password-lost' ) );
            exit;
        }
    }

     /* A shortcode for rendering the form used to initiate the password reset.
     *
     * @param  array   $attributes  Shortcode attributes.
     * @param  string  $content     The text content for shortcode. Not used.
     *
     * @return string  The shortcode output
     */
    public function render_password_lost_form( $attributes, $content = null ) {

        // Parse shortcode attributes
        $default_attributes = array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );
     
        if ( is_user_logged_in() ) {
            return __( 'You are already signed in.', 'personalize-login' );
        } else {
            // Retrieve possible errors from request parameters
            $attributes['errors'] = array();
            if ( isset( $_REQUEST['errors'] ) ) {
                $error_codes = explode( ',', $_REQUEST['errors'] );
             
                foreach ( $error_codes as $error_code ) {
                    $attributes['errors'] []= $this->get_error_message( $error_code );
                }
            }
            // Check if the user just request resend confirmation code email
            $attributes['resend_cc'] = isset( $_REQUEST['request']) && $_REQUEST['request'] == 'confirmation_code' ;
            return $this->get_template_html( 'password_lost_form', $attributes );
        }
    }

    /**
     * Initiates password reset.
     */
    public function do_password_lost() {
        if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
            if(isset($_POST['request']) && 'resend' == $_POST['request']){
                $errors = $this->resend_confirmation_email($_POST);
                //print_r($errors);
                if (isset($errors) || is_wp_error( $errors ) ) {
                    $redirect_url = home_url( 'member-password-lost' );
                    // Errors found
                    $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
                    $redirect_url = add_query_arg( 'request', 'confirmation_code', $redirect_url );
                } else {
                    // Email sent
                    $redirect_url = home_url( 'member-login' );
                    $redirect_url = add_query_arg( 'checkemail', 'resended', $redirect_url );
                }   
                wp_redirect( $redirect_url );           
                exit;
            } else {
                $errors = retrieve_password();
                if ( is_wp_error( $errors ) ) {
                    // Errors found
                    $redirect_url = home_url( 'member-password-lost' );
                    $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
                } else {
                    // Email sent
                    $redirect_url = home_url( 'member-login' );
                    $redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
                }
         
                wp_redirect( $redirect_url );
                exit;
            }
        }
    }

    /**
     * Returns the message body for the password reset mail.
     * Called through the retrieve_password_message filter.
     *
     * @param string  $message    Default mail message.
     * @param string  $key        The activation key.
     * @param string  $user_login The username for the user.
     * @param WP_User $user_data  WP_User object.
     *
     * @return string   The mail message to send.
     */
    public function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
        // Create new message
        $msg  = __( 'Hello!', 'personalize-login' ) . "\r\n\r\n";
        $msg .= sprintf( __( 'You asked us to reset your password for your account using the email address %s.', 'personalize-login' ), $user_login ) . "\r\n\r\n";
        $msg .= __( "If this was a mistake, or you didn't ask for a password reset, just ignore this email and nothing will happen.", 'personalize-login' ) . "\r\n\r\n";
        $msg .= __( 'To reset your password, visit the following address:', 'personalize-login' ) . "\r\n\r\n";
        $msg .= site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
        $msg .= __( 'Thanks!', 'personalize-login' ) . "\r\n";
     
        return $msg;
    }

    /**
     * Redirects to the custom password reset page, or the login page
     * if there are errors.
     */
    public function redirect_to_custom_password_reset() {
        if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {

            // Verify key / login combo
            $user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
            if ( ! $user || is_wp_error( $user ) ) {
                if ( $user && $user->get_error_code() === 'expired_key' ) {
                    wp_redirect( home_url( 'member-login?login=expiredkey' ) );
                } else {
                    wp_redirect( home_url( 'member-login?login=invalidkey' ) );
                }
                exit;
            }

            if('ac' == $_GET['action']){
                update_user_meta($user->ID, 'cemail', 'confirmed'); 
                wp_redirect( home_url( 'member-login?email=confirmed' ) );
                exit;
            }           
     
            $redirect_url = home_url( 'member-password-reset' );
            $redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
            $redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
     
            wp_redirect( $redirect_url );
            exit;
        }
    }

    /**
     * A shortcode for rendering the form used to reset a user's password.
     *
     * @param  array   $attributes  Shortcode attributes.
     * @param  string  $content     The text content for shortcode. Not used.
     *
     * @return string  The shortcode output
     */
    public function render_password_reset_form( $attributes, $content = null ) {
        // Parse shortcode attributes
        $default_attributes = array( 'show_title' => false );
        $attributes = shortcode_atts( $default_attributes, $attributes );
     
        if ( is_user_logged_in() ) {
            return __( 'You are already signed in.', 'personalize-login' );
        } else {
            if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
                $attributes['login'] = $_REQUEST['login'];
                $attributes['key'] = $_REQUEST['key'];
     
                // Error messages
                $errors = array();
                if ( isset( $_REQUEST['error'] ) ) {
                    $error_codes = explode( ',', $_REQUEST['error'] );
     
                    foreach ( $error_codes as $code ) {
                        $errors []= $this->get_error_message( $code );
                    }
                }
                $attributes['errors'] = $errors;
     
                return $this->get_template_html( 'password_reset_form', $attributes );
            } else {
                return __( 'Invalid password reset link.', 'personalize-login' );
            }
        }
    }

    /**
     * Resets the user's password if the password reset form was submitted.
     */
    public function do_password_reset() {
        if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
            $rp_key = $_REQUEST['rp_key'];
            $rp_login = $_REQUEST['rp_login'];
     
            $user = check_password_reset_key( $rp_key, $rp_login );
     
            if ( ! $user || is_wp_error( $user ) ) {
                if ( $user && $user->get_error_code() === 'expired_key' ) {
                    wp_redirect( home_url( 'member-login?login=expiredkey' ) );
                } else {
                    wp_redirect( home_url( 'member-login?login=invalidkey' ) );
                }
                exit;
            }
     
            if ( isset( $_POST['pass1'] ) ) {
                if ( $_POST['pass1'] != $_POST['pass2'] ) {
                    // Passwords don't match
                    $redirect_url = home_url( 'member-password-reset' );
     
                    $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
                    $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
                    $redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );
     
                    wp_redirect( $redirect_url );
                    exit;
                }
     
                if ( empty( $_POST['pass1'] ) ) {
                    // Password is empty
                    $redirect_url = home_url( 'member-password-reset' );
     
                    $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
                    $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
                    $redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
     
                    wp_redirect( $redirect_url );
                    exit;
                }
     
                // Parameter checks OK, reset password
               
               
               $UserId = get_user_meta($user->id, 'f6UserId', true);
               $accountsId = get_user_meta($user->id, 'f6Id', true);
               $email = get_userdata($user->id)->user_email;
               $admin_token = authenticate();
              // echo $f6UserId;
             //  echo $f6Id;
             //  echo $token;
             //  echo $mail; 

              AdminChangePassword($UserId, $accountsId, $admin_token, $email, $_POST['pass1']);
              logout($admin_token);              
          
                reset_password( $user, $_POST['pass1'] );
                wp_redirect( home_url( 'member-login?password=changed' ) );
            } else {
                echo "Invalid request.";
            }
     
            exit;
        }
    }

    private function custom_wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
       if ( $deprecated !== null ) {
            _deprecated_argument( __FUNCTION__, '4.3.1' );
        }

        global $wpdb, $wp_hasher;
        $user = get_userdata( $user_id );
        $user_country = get_user_meta($user->ID, 'country', true);
        $user_city = get_user_meta($user->ID, 'city', true);
        $user_region = get_user_meta($user->ID, 'region', true);
        $user_ip = get_user_meta($user->ID, 'ip-address', true);

        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        if ( 'user' !== $notify ) {
            $switched_locale = switch_to_locale( get_locale() );
            $message  = sprintf( __( 'There\'s a new user registration on your site %s:' ), $blogname ) . "\r\n\r\n";
            $message .= sprintf( __( 'Username: %s %s' ), $user->first_name, $user->last_name ) . "\r\n\r\n";
            $message .= sprintf( __( 'Email: %s' ), $user->user_email ) . "\r\n";
            $message .= sprintf( __( 'IP: %s' ),$user_ip ) . "\r\n";
            $message .= sprintf( __( 'City: %s' ),$user_city ) . "\r\n";
            $message .= sprintf( __( 'Country: %s' ),$user_country ) . "\r\n";
            $message .= sprintf( __( 'Region: %s' ),$user_region ) . "\r\n";

            @wp_mail( get_option( 'admin_email' ), sprintf( __( '[%s] New User Registration' ), $blogname ), $message );

            if ( $switched_locale ) {
                restore_previous_locale();
            }
        }

        // `$deprecated was pre-4.3 `$plaintext_pass`. An empty `$plaintext_pass` didn't sent a user notification.
        //if ( 'admin' === $notify || ( empty( $deprecated ) && empty( $notify ) ) ) {
          //  return;
        //}

        // Generate something random for a password reset key.
        $key = wp_generate_password( 20, false );
        // 
        update_user_meta($user->ID, 'cemail', $key);
        
        $first_name = get_user_meta($user->ID, 'first_name', true);
		$last_name = get_user_meta($user->ID, 'last_name', true);
		update_user_meta($user->ID, 'billing_first_name', $first_name);
        update_user_meta($user->ID, 'billing_last_name', $last_name);
        update_user_meta($user->ID, 'billing_country', $user_country);
        update_user_meta($user->ID, 'billing_city', $user_city);

               
        /** This action is documented in wp-login.php */
        do_action( 'retrieve_password_key', $user->user_login, $key );

        // Now insert the key, hashed, into the DB.
        if ( empty( $wp_hasher ) ) {
            require_once ABSPATH . WPINC . '/class-phpass.php';
            $wp_hasher = new PasswordHash( 8, true );
        }
        $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
        $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

        $switched_locale = switch_to_locale( get_user_locale( $user ) );

        $message = sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
        $message .= __('To activate your account, visit the following address:') . "\r\n\r\n";
        $message .= '<' . network_site_url("wp-login.php?action=ac&key=$key&login=" . rawurlencode($user->user_login), 'login') . ">\r\n\r\n";
        $message .= __('To set your password, visit the following address:') . "\r\n\r\n";
        $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login') . ">\r\n\r\n";

        $message .= wp_login_url() . "\r\n";

        wp_mail($user->user_email, sprintf(__('[%s] Your username and password info'), $blogname), $message);

        if ( $switched_locale ) {
            restore_previous_locale();
        }

    }   

    public function is_email_confirmed($username, $password ) {

        if ( ! username_exists( $username ) ) {
            return;
        }
        $user = get_user_by('login', $username);
        $cemail = get_user_meta($user->ID, 'cemail', true);
        if('confirmed' != $cemail){
            wp_redirect( home_url( 'member-login?email=not_confirmed' ) );
            exit;
        }
        if(function_exists ( 'tryLoginToFSApi' )){
            tryLoginToFSApi($user->user_email, $password, $user->ID);
        } else {
            echo "Function not exist, please activate plugin";
            die();
        }
    }

    private function resend_confirmation_email( $post ) {

        global $errors;

        if ( empty( $post['user_login'] ) ) {
            $errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));
        } elseif ( strpos( $post['user_login'], '@' ) ) {
            $user = get_user_by( 'email', trim( wp_unslash( $post['user_login'] ) ) );
            if ( empty( $user ) )
                $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
        } else {
            $login = trim($post['user_login']);
            $user = get_user_by('login', $login);
        }   

        if ( $errors->get_error_code() )
            return $errors;

        if ( !$user ) {
            $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or email.'));
            return $errors;
        }           

        // The blogname option is escaped with esc_html on the way into the database in sanitize_option
        // we want to reverse this for the plain text arena of emails.
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        // Generate something random for a password reset key.
        $key = get_user_meta($user->ID, 'cemail', true);
               
        $switched_locale = switch_to_locale( get_user_locale( $user ) );

        $message = sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
        $message .= __('To activate your account, visit the following address:') . "\r\n\r\n";
        $message .= '<' . network_site_url("wp-login.php?action=ac&key=$key&login=" . rawurlencode($user->user_login), 'login') . ">\r\n\r\n";

        $message .= wp_login_url() . "\r\n";

        wp_mail($user->user_email, sprintf(__('[%s] Your username and password info'), $blogname), $message);

        if ( $switched_locale ) {
            restore_previous_locale();
        }

    }   

}

 
// Initialize the plugin
$personalize_login_pages_plugin = new Personalize_Login_Plugin();

// Create the custom pages at plugin activation
register_activation_hook( __FILE__, array( 'Personalize_Login_Plugin', 'plugin_activated' ) );