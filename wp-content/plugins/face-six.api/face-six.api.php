<?php

/**
 * Plugin Name:       Face-six.api
 * Description:       A plugin that .
 * Version:           1.0.0
 * Author:            HS
 * License:           GPL
 * Text Domain:       face-six.com
 */


include_once 'FSixApiService.php';


add_action('woocommerce_thankyou', 'payment_success_redirect', 112, 1);
add_action('paypal_ipn_for_wordpress_txn_type_subscr_payment', 'subscr_payment_ipn', 10, 1);
add_action('paypal_ipn_for_wordpress_txn_type_subscr_signup',  'subscr_started_ipn', 10, 1);



// will run on payments IPN form PayPal
function subscr_payment_ipn($posted)
{

    /*$posted = Array // test IPN
    (
        'transaction_subject' => '2117 (Order 2116) - FA6 Alert - Basic',
        'payment_date' => '04:28:11 Jan 29, 2019 PST',
        'txn_type' => 'subscr_payment',
        'subscr_id' => 'I-HKFB37F0HJ08',
        'last_name' => 'buyer',
        'residence_country' => 'IL',
        'item_name' => 'Subscription 2117 (Order 2116) - FA6 Alert - Basic',
        'payment_gross' => '1.00',
        'mc_currency' => 'USD',
        'business' => 'alexadoniev-facilitator@gmail.com',
        'payment_type' => 'instant',
        'protection_eligibility' => 'Eligible',
        'verify_sign' => 'Al4elZGnFKMawz1nSAJZrlse2881AjGISCd85QP-3rxz47vVNzImWmon',
        'payer_status' => 'verified',
        'test_ipn' => '1',
        'payer_email' => 'alexadoniev-buyer@gmail.com',
        'txn_id' => '6LE16662CG791273U',
        'receiver_email' => 'alexadoniev-facilitator@gmail.com',
        'first_name' => 'test',
        'invoice' => 'WC-2116',
        'payer_id' => '7F2BPDHC2X6PY',
        'receiver_id' => 'X4NHQB9TYCX2J',
        'contact_phone' => '050-617-5784',
        'payment_status' => 'Completed',
        'payment_fee' => '0.33',
        'mc_fee' => '0.33',
        'mc_gross' => '1.00',
        'custom' => '{"order_id":2116,"order_key":"wc_order_lU37h6yALmQ0W","subscription_id":2117,"subscription_key":"wc_order_5c5046b21f2ce"}',
        'charset' => 'windows-1255',
        'notify_version' => '3.9',
        'ipn_track_id' => 'cf4079954335',
        'IPN_status' => 'Verified',
    );*/


    $payment_info    = json_decode($posted['custom'], true);
    $order_id        = $payment_info['order_id'];
    $order           = wc_get_order($order_id);
    $item            = current($order->get_items());
    $product         = $item->get_product();

    try {
        $f6_account_id = wc_get_order_item_meta($item->get_id(), 'f6acc', true); // get face-six account, that did payment
    } catch (Exception $e) {
        echo $e->getMessage();
    }

    date_default_timezone_set('Israel');
    $start_date = date('Y-m-d H:i:s');
    $period     = WC_Subscriptions_Product::get_period($product);//day, month, year...
    $interval   = WC_Subscriptions_Product::get_interval($product); //amount
    $end_date   = date('Y-m-d H:i:s', strtotime($start_date . " +" . $interval . " " . $period));


    try {
        $admin_token  = FSixApiService::authenticate(); // admin auth
                        FSixApiService::subscription($f6_account_id, $admin_token, $start_date, $end_date);//update subscription data

    } catch (ErrorException $e) {
        echo $e->getMessage();
    }



}

// will run on new subscription IPN form PayPal
function subscr_started_ipn($posted)
{

    /*$posted = Array // test IPN
    (
        'txn_type' => 'subscr_signup',
        'subscr_id' => 'I-HKFB37F0HJ08',
        'last_name' => 'buyer',
        'residence_country' => 'IL',
        'mc_currency' => 'USD',
        'item_name' => 'Subscription 2117 (Order 2116) - FA6 Alert - Basic',
        'business' => 'alexadoniev-facilitator@gmail.com',
        'amount3' => '1.00',
        'recurring' => '1',
        'verify_sign' => 'ALFiv1FWqJJt4TzHaCFVB4Iwp9n2A5ykmEv1rYr9xK4kFnpfvHNfLGds',
        'payer_status' => 'verified',
        'test_ipn' => '1',
        'payer_email' => 'alexadoniev-buyer@gmail.com',
        'first_name' => 'test',
        'receiver_email' => 'alexadoniev-facilitator@gmail.com',
        'payer_id' => '7F2BPDHC2X6PY',
        'invoice' => 'WC-2116',
        'reattempt' => '1',
        'subscr_date' => '04:28:10 Jan 29, 2019 PST',
        'custom' => '{"order_id":2116,"order_key":"wc_order_lU37h6yALmQ0W","subscription_id":2117,"subscription_key":"wc_order_5c5046b21f2ce"}',
        'charset' => 'windows-1255',
        'notify_version] => 3.9',
        'period3' => '1 D',
        'mc_amount3' => '1.00',
        'ipn_track_id' => 'cf4079954335',
        'IPN_status' => 'Verified',
    );*/
    

    $payment_info    = json_decode($posted['custom'], true);

    $order_id        = $payment_info['order_id'];
    //$subscription_id = $payment_info['subscription_id'];

    $order    = wc_get_order($order_id);
    $item     = current($order->get_items());

    //$user_id = $order->customer_id;

        try {
            $admin_token     = FSixApiService::authenticate(); // admin auth
            $account_name    = FSixApiService::generateRandomString(); // for testing
            $f6_account_id   = FSixApiService::accounts($admin_token, $account_name); // create new account

            try {
                wc_update_order_item_meta($item->get_id(), 'f6acc', $f6_account_id); // associate created account with order
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
}




// test hook 'paypal_ipn_for_wordpress_txn_type_subscr_signup' using page 'new_sub'
function new_subscr()
{
    if (is_page('new_sub')) {
        do_action('paypal_ipn_for_wordpress_txn_type_subscr_signup');
    }
}
add_action('template_redirect', 'new_subscr');


// test hook 'paypal_ipn_for_wordpress_txn_type_subscr_payment' using page 'new_payment'
function new_payment()
{
    if (is_page('new_payment')) {
        do_action('paypal_ipn_for_wordpress_txn_type_subscr_payment');
    }
}
add_action('template_redirect', 'new_payment');


