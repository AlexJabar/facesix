<?php

class FSixApiService
{

    const DOMAIN = "http://34.250.98.115:3000";


    /**
     * @throws ErrorException
     */
    public static function authenticate()
    {

        $data = array(
            "email" => 'Admin@Admin',
            "password" => "@@@dmin",
            "clientid" => "1",
            "locationid" => "1",
        );

        $data_string = json_encode($data);

        $ch = curl_init(self::DOMAIN . "/fa6_api/authenticate");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // to write into var
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);

        $result = json_decode($result, true);

        if ($result['status'] == 'success') {
            return $result['token'];
        } else
            throw new ErrorException("Fail login as admin");
    }


    /**
     * @throws ErrorException
     */
    public static function accounts($admin_token, $name)
    { // Create new Account

        $data = array(
            "accountname" => $name,
            "type" => "Standard",
            "timeout" => "5000000",
            // "applications" => "event"
        );
        $data_string = json_encode($data);

        $ch = curl_init(self::DOMAIN . "/fa6_api/accounts");

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($admin_token))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if ($result['status'] == 'failed') {
            throw new ErrorException("Error trying to create account");
        } else
            return $result['id'];
    }



    public static function applications($admin_token, $accountsId)
    {

        $data = array(
            "application" => 'event');
        $data_string = json_encode($data);

        $ch = curl_init(self::DOMAIN . "/fa6_api/accounts/" . $accountsId . "/applications");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($admin_token))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);

    }


    /**
     * @throws ErrorException
     */
    public static function subscription($accountsId, $admin_token, $start_time, $end_time)
    {

        $data = array(
            'tariffid'      => 1,
            'startdatetime' => $start_time,
            'enddatetime'   => $end_time,
            'maxcameras'    => 2);

        $data_string = json_encode($data);
        $url = self::DOMAIN . "/fa6_api/accounts/" . $accountsId . "/subscriptions/";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($admin_token))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);

        if ($result['status'] == 'failed') {
            throw new ErrorException($result['status']);
        } else
            return $result['status'];

    }


    public static function accountsSet($accountsId, $admin_token)
    { //setting for User

        $data = array(
            'settings' => array(
                0 => array(
                    'value' => 1000,
                    'name' => 'interval',
                ),
                1 => array(
                    'value' => 1,
                    'name' => 'minenrolfacequality',
                ),
                2 => array(
                    'value' => 1,
                    'name' => 'minprobefacequality',
                ),
                3 => array(
                    'value' => 50,
                    'name' => 'minprobematchscore',
                ),
                4 => array(
                    'value' => 10,
                    'name' => 'maxmatchcount',
                ),
                5 => array(
                    'value' => 'no',
                    'name' => 'detectallfeaturepoints',
                ),
                6 => array(
                    'value' => 'low',
                    'name' => 'matchingspeed',
                ),
                7 => array(
                    'value' => 1,
                    'name' => 'minimalinteroculardistance',
                ),
                8 => array(
                    'value' => 1,
                    'name' => 'confidencethreshold',
                ),
                9 => array(
                    'value' => 45,
                    'name' => 'maximalroll',
                ),
                10 => array(
                    'value' => 45,
                    'name' => 'maximalyaw',
                ),
            ),
        );

        $data_string = json_encode($data);
        $url = self::DOMAIN . "/fa6_api/accounts/" . $accountsId . "/settings/";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($admin_token))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
    }


    public static function createUser($accountsId, $mail, $password, $admin_token)
    {
        //$curl = curl_init();
        $data = array(
            "email" => $mail,
            "password" => $password,
            "type" => "super",
        );
        $data_string = json_encode($data);

        $url = self::DOMAIN . "/fa6_api/accounts/";
        $url = $url . "" . $accountsId . "/users/";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($admin_token))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result['id'];
    }

    public static function authenticateUser($mail, $password)
    {
        $data = array(
            "email" => $mail,
            "password" => $password,
            "clientid" => "1",
            "locationid" => "1"
        );
        $data_string = json_encode($data);

        $ch = curl_init( self::DOMAIN . "/fa6_api/authenticate");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);

        $result = json_decode($result, true);
        if ($result) {
            return $result['token'];
        } else {
            return null;
        }
    }


    public static function location($userToken)
    {
        $data = array(
            "name" => 'default',
            "parentid" => 1,);
        $data_string = json_encode($data);
        $ch = curl_init( self::DOMAIN . "/fa6_api/locations/");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($userToken))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        // print_r($result);
        //   echo "<br>";
        return $result["id"];
    }

    public static function clients($userToken)
    {
        $data = array(
            "name" => 'default',
            "url" => 'http://localhost:1414');
        $data_string = json_encode($data);
        $ch = curl_init( self::DOMAIN . "/fa6_api/clients/");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($userToken))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        //   print_r($result);
        return $result["id"];
    }


    public static function settings($userToken, $cliId)
    {

        $data = array(
            'settings' =>
                array(
                    0 =>
                        array(
                            'value' => 1,
                            'name' => 'minprobefaceuality',
                        ),
                    1 =>
                        array(
                            'value' => 50,
                            'name' => 'minprobematchscore',
                        ),
                    2 =>
                        array(
                            'value' => 1000,
                            'name' => 'interval',
                        ),
                    3 =>
                        array(
                            'value' => 'no',
                            'name' => 'detectallfeaturepoints',
                        ),
                    4 =>
                        array(
                            'value' => 'low',
                            'name' => 'matchingspeed',
                        ),
                    5 =>
                        array(
                            'value' => 1,
                            'name' => 'minimalinteroculardistance',
                        ),
                    6 =>
                        array(
                            'value' => 1,
                            'name' => 'confidencethreshold',
                        ),
                    7 =>
                        array(
                            'value' => 45,
                            'name' => 'maximalroll',
                        ),
                    8 =>
                        array(
                            'value' => 45,
                            'name' => 'maximalyaw',
                        ),
                ),
            'clientid' => $cliId,
        );
        $data_string = json_encode($data);
        $ch = curl_init( self::DOMAIN . "/fa6_api/settings");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($userToken))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        //  print_r($result);

    }


    public static function cameras($userToken, $cliId, $locId)
    {

        $data = array(
            'name' => 'USB camera',
            'url' => '0',
            'clientid' => $cliId,
            'locationid' => $locId,
        );
        $data_string = json_encode($data);
        $ch = curl_init( self::DOMAIN . "/fa6_api/cameras/");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                'authorization: ' . htmlspecialchars($userToken))
        );
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        //  print_r($result);

    }


    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}
